INSERT INTO rb.locales
    (code, default_locale)
VALUES
    ('de-DE', true)
;

INSERT INTO rb.slugs
    (locale_code, slug)
VALUES
    ('de-DE', 'de')
;

INSERT INTO rb.aspects_texts(translation_code, aspect_code, name)
VALUES
    ('de-DE', 'main', 'haupt'), ('de-DE', 'more', 'mehr'),
    ('de-DE', 'banner', 'Banner'),
    ('de-DE', 'inside', 'innen'),    ('de-DE', 'outside',  'außen'),
    ('de-DE', 'top', 'oben' ),    ('de-DE', 'bottom', 'unten' ),
    ('de-DE', 'left', 'links'),    ('de-DE', 'right', 'rechts'),
    ('de-DE', 'front', 'vorne'),    ('de-DE', 'back', 'hinten')
;

CREATE EXTENSION pgcrypto;
CREATE EXTENSION ltree;

--CREATE DOMAIN rb.text AS varchar COLLATE "und-x-icu" CHECK(VALUE <> '');
CREATE DOMAIN rb.text AS varchar CHECK(VALUE <> '');
CREATE DOMAIN rb.text_array AS varchar[] CHECK(length(array_to_string(VALUE, '')) > 1);
CREATE DOMAIN rb.email AS varchar CHECK (VALUE ~* '^[A-Za-z0-9._%-]+@[A-Za-z0-9.-]+[.][A-Za-z]+$');
CREATE DOMAIN rb.postal_address AS rb.text_array CHECK(length(array_to_string(VALUE, '')) > 1);

CREATE TYPE rb.fkey AS (
    fkey_table_name text,
    fkey_name text,
    src_table_name text,
    src_key_name text,
    data_type text,
    fkey_table_schema text,
    src_table_schema text
);

-- Retrieves foreign keys and their source.
-- Uses maintenance table `information_schema`.
CREATE VIEW rb.fkeys AS
SELECT
    fkey_table_name,
    fkey_name,
    src_table_name,
    src_key_name,
    data_type,
    fkey_table_schema,
    src_table_schema
FROM (
SELECT
    c.data_type data_type,
    tc.table_schema fkey_table_schema,
    tc.table_name fkey_table_name, 
    kcu.column_name fkey_name,
    ccu.table_schema src_table_schema,
    ccu.table_name src_table_name,
    ccu.column_name src_key_name
FROM
    information_schema.table_constraints AS tc
JOIN
    -- kcu
    information_schema.key_column_usage AS kcu
    ON tc.constraint_name = kcu.constraint_name
    AND tc.table_schema = kcu.table_schema
JOIN
    -- ccu
    information_schema.constraint_column_usage AS ccu
    ON ccu.constraint_name = tc.constraint_name
    AND ccu.table_schema = tc.table_schema
JOIN
    -- c
    information_schema.columns c
    ON c.table_name = ccu.table_name
    AND c.table_schema = ccu.table_schema
    AND c.column_name = ccu.column_name
WHERE
    -- foreign key
    tc.constraint_type = 'FOREIGN KEY'
    -- schema = 'rb'
    AND kcu.constraint_schema = 'rb'
    -- exclude *migrations tables
    AND ccu.table_name NOT LIKE '%migrations'
ORDER BY
    -- foreign key: schema > table name > name
    fkey_table_schema,
    fkey_table_name,
    fkey_name
) AS fkeys;

-- Type for aggregating foreign keys grouped by source table.
CREATE TYPE rb.fkey_grouped_by_src AS (
    fkey_table_name text,
    fkey_table_schema text,
    fkey_name text,
    data_type text,
    src_key_name text
    -- must be collected separately:
    -- src_table_name text,
    -- src_table_schema text
);

-- Retrieves foreign keys grouped by source tables.
-- Uses maintenance table `information_schema` and a custom TYPE.
CREATE VIEW rb.fkeys_grouped_by_src AS
SELECT
    src_table_schema,
    src_table_name,
    src_fkeys
FROM (
    SELECT
        fk.src_table_name,
        fk.src_table_schema,
        array_agg((
            fk.fkey_table_schema,
            fk.fkey_table_name,
            fk.fkey_name,
            fk.data_type,
            fk.src_key_name
        )::rb.fkey_grouped_by_src) src_fkeys
    FROM
        rb.fkeys fk
    WHERE
        fk.fkey_table_schema = 'rb'
    GROUP BY
        fk.src_table_schema,
        fk.src_table_name
) AS fkeys;



CREATE TYPE rb.fkey_grouped_by_dst AS (
    src_table_schema text,
    src_table_name text,
    src_key_name text,
    data_type text,
    fkey_name text
);

CREATE VIEW rb.fkeys_grouped_by_dst AS
SELECT
    dst_table_schema,
    dst_table_name,
    dst_fkeys
FROM (
    SELECT
        fk.fkey_table_schema dst_table_schema,
        fk.fkey_table_name dst_table_name,
        array_agg((
            fk.src_table_schema,
            fk.src_table_name,
            fk.src_key_name,
            fk.data_type,
            fk.fkey_name
        )::rb.fkey_grouped_by_dst) dst_fkeys
    FROM
        rb.fkeys fk
    WHERE
        fk.src_table_schema = 'rb'
    GROUP BY
        fk.fkey_table_schema,
        fk.fkey_table_name
) as fkeys;



CREATE TYPE rb.pkey AS (
    pkey_table_name text,
    pkey_name text,
    pkey_position integer,
    src_table_name text,
    src_key_name text,
    data_type text,
    pkey_table_schema text,
    src_table_schema text
);


-- Retrieves primary keys and maybe their source if they are a both a primary and a foreign key.
-- Uses maintenance table `information_schema`.
CREATE VIEW rb.pkeys AS
SELECT
    pkey_table_name,
    pkey_name,
    pkey_position,
    src_table_name,
    src_key_name,
    data_type,
    pkey_table_schema,
    src_table_schema
FROM (
SELECT
    -- table name
    kcu.table_name pkey_table_name,
    -- primary key name
    kcu.column_name pkey_name,
    -- type
    c.data_type data_type,
    -- primary position
    kcu.ordinal_position pkey_position,
    -- constraint name
    tc.constraint_name,
    -- table schema
    c.table_schema pkey_table_schema,
    -- src: table name, table schema, primary key
    COALESCE(fk.src_table_schema, NULL) src_table_schema,
    COALESCE(fk.src_table_name, NULL) src_table_name,
    COALESCE(fk.src_key_name, NULL) src_key_name
FROM
    -- tc: table constraints
    information_schema.table_constraints tc
JOIN
    -- kcu: key column usage
    information_schema.key_column_usage kcu
    ON tc.constraint_name = kcu.constraint_name
    AND tc.constraint_schema = kcu.constraint_schema
JOIN
    -- c: columns
    information_schema.columns c
    ON kcu.table_name = c.table_name
    AND tc.constraint_schema = c.table_schema
    AND kcu.column_name = c.column_name
LEFT JOIN
    -- fk: foreign keys
    rb.fkeys fk
    ON fk.fkey_table_schema = tc.constraint_schema
    AND fk.fkey_table_name = kcu.table_name
    AND fk.fkey_name = kcu.column_name
WHERE
    -- primary key
    tc.constraint_type = 'PRIMARY KEY'
    -- schema = 'rb'
    AND kcu.constraint_schema = 'rb'
    -- exclude *migrations tables
    AND kcu.table_name NOT LIKE '%migrations'
ORDER BY
    -- schema > table name > position
    pkey_table_schema,
    pkey_table_name,
    pkey_position
) AS pkeys;


-- Creates a table constraint for referencing exactly one foreign table as destination.
-- It's basically a XOR check on the foreign key of the given source table.
-- Each destination table must have exactly one primary key column.
CREATE FUNCTION rb.create_check_exactly_one_dst(
    src_table_name text,
    exclude_fkey_names text[],
    VARIADIC dst_table_names text[]
)
RETURNS void
AS $$
#variable_conflict use_variable
DECLARE
    fkeys rb.fkey_grouped_by_dst[] = '{}';
    fkey_current_1 rb.fkey_grouped_by_dst;
    fkey_current_2 rb.fkey_grouped_by_dst;
    constraint_str text;
BEGIN
    fkeys := (
        SELECT
            array_agg((
                (fk.dfkey).src_table_schema,
                (fk.dfkey).src_table_name,
                (fk.dfkey).src_key_name,
                (fk.dfkey).data_type,
                (fk.dfkey).fkey_name
            )::rb.fkey_grouped_by_dst)
        FROM (
            SELECT
                unnest(dst_fkeys) dfkey,
                fk.dst_table_schema dst_table_schema,
                fk.dst_table_name dst_table_name
            FROM
                rb.fkeys_grouped_by_dst fk
            WHERE
                fk.dst_table_schema = 'rb'
                AND fk.dst_table_name = src_table_name
            ) AS fk
        WHERE
            (fk.dfkey).src_table_schema = 'rb'
            AND (fk.dfkey).src_table_name = ANY(dst_table_names)
            AND (fk.dfkey).fkey_name != ANY(exclude_fkey_names)
    );

    -- Create string for XOR check.
    constraint_str := format('
        ALTER TABLE rb.%1$s
            DROP CONSTRAINT IF EXISTS exactly_one_dst;

        ALTER TABLE rb.%1$s
            ADD CONSTRAINT exactly_one_dst CHECK (
    ', src_table_name);

    FOREACH fkey_current_1 IN ARRAY fkeys LOOP
        IF fkey_current_1 != fkeys[1] THEN
            constraint_str := constraint_str || ' OR';
        END IF;

        constraint_str := constraint_str || format(
            ' (%I IS NOT NULL',
            fkey_current_1.fkey_name
        );

        FOREACH fkey_current_2 in ARRAY fkeys LOOP
            IF fkey_current_1 != fkey_current_2 THEN
                constraint_str := constraint_str || format(
                    ' AND %I IS NULL',
                    fkey_current_2.fkey_name
                );
            END IF;
        END LOOP;

        constraint_str := constraint_str || ')';
    END LOOP;

    constraint_str := constraint_str || ');';

    -- Create constraint.
    EXECUTE constraint_str;
END $$
LANGUAGE plpgsql;


-- The foreign key for create_index_on_slugs() is actually a stripped primary key.
CREATE TYPE rb.fkey_slug AS(
    pkey_table_schema text,
    pkey_table_name text,
    pkey_name text,
    data_type text
);

-- Make a table have associated slugs.
-- Adds given source tables to table `slugs`.
-- Source table must have exactly one primary key column.
CREATE FUNCTION rb.register_slugs(
    src_table_name text,
    src_item_name text
)
RETURNS void
AS $$
#variable_conflict use_variable
DECLARE
    fkeys rb.fkey_slug[];
    fkeys_len integer;
    fkey_current integer := 1;
    all_src_tables text[] := '{}';
BEGIN
    -- Get all primary keys from source table.
    fkeys := (
        SELECT
            array_agg((
                pkey_table_schema,
                pkey_table_name,
                pkey_name,
                data_type
            )::rb.fkey_slug)
        FROM
            rb.pkeys pk
        WHERE
            pk.pkey_table_schema = 'rb'
            AND pk.pkey_table_name = src_table_name
    )::rb.fkey_slug[];

    fkeys_len := array_length(fkeys, 1);

    -- Add foreign keys to table `slugs`.
    -- FIXME: Create proper reference from multiple foreign keys
    WHILE fkey_current = fkeys_len LOOP        
        EXECUTE format('
            ALTER TABLE rb.slugs
                ADD %I %s REFERENCES %I.%I(%I) ON DELETE CASCADE ON UPDATE CASCADE;
        ',
            src_item_name || '_' || fkeys[fkey_current].pkey_name,
            fkeys[fkey_current].data_type::regtype,
            fkeys[fkey_current].pkey_table_schema,
            fkeys[fkey_current].pkey_table_name,
            fkeys[fkey_current].pkey_name
        );

        fkey_current := fkey_current + 1;
    END LOOP;

    -- Rebuild constraint to check for only referencing one foreign table at a time.
    all_src_tables := (
        SELECT
            array_agg(fk.src_table_name)
        FROM
            rb.fkeys fk
        WHERE
            fk.fkey_table_schema = 'rb'
            AND fk.fkey_table_name = 'slugs'
            AND fk.fkey_name != 'translation_code'
    );

    EXECUTE rb.create_check_exactly_one_dst('slugs', array['translation_code'], VARIADIC all_src_tables);
END $$
LANGUAGE plpgsql;



-- Sets column `updated_at` when the table is modified.
-- https://github.com/diesel-rs/diesel/issues/91#issue-126464176
CREATE FUNCTION rb.set_updated_at()
RETURNS trigger AS $$
BEGIN
    IF (
        NEW IS DISTINCT FROM OLD AND
        NEW.updated_at IS NOT DISTINCT FROM OLD.updated_at
    ) THEN
        NEW.updated_at := current_timestamp;
    END IF;
    RETURN NEW;
END
$$ LANGUAGE plpgsql;

-- Sets column `published_at` when column `published` is set to true.
CREATE OR REPLACE FUNCTION rb.set_published_at()
RETURNS TRIGGER AS $$
BEGIN
    IF (
        OLD.published_at IS NOT NULL AND
        NEW.published IS TRUE AND
        NEW.published IS DISTINCT FROM OLD.published
    ) THEN
        NEW.published_at := current_timestamp;
    END IF;
    RETURN NEW;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION rb.add_published(src_table_name text)
RETURNS void AS $$
BEGIN
    EXECUTE format('
        ALTER TABLE rb.%1$s
        ADD published BOOLEAN NOT NULL DEFAULT FALSE,
        ADD published_at timestamp DEFAULT NULL;
        
        CREATE TRIGGER set_published_at
        BEFORE UPDATE ON rb.%1$s
                FOR EACH ROW EXECUTE PROCEDURE rb.set_published_at();
        ',
        src_table_name
    );
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION rb.add_created(src_table_name text)
RETURNS void AS $$
BEGIN
    EXECUTE format('
        ALTER TABLE rb.%I
        ADD
            created_at timestamp NOT NULL DEFAULT current_timestamp',
        src_table_name
    );
END;
$$ LANGUAGE plpgsql;


CREATE OR REPLACE FUNCTION rb.set_updated(src_table_name text)
RETURNS void AS $$
BEGIN
    EXECUTE format('
        CREATE TRIGGER set_updated_at
        BEFORE UPDATE ON rb.%1$s
                FOR EACH ROW EXECUTE PROCEDURE rb.set_updated_at()
        ',
        src_table_name
    );
END;
$$ LANGUAGE plpgsql;


CREATE OR REPLACE FUNCTION rb.add_updated(src_table_name text)
RETURNS void AS $$
BEGIN
    EXECUTE format('
        ALTER TABLE rb.%1$s
        ADD
            updated_at timestamp NOT NULL DEFAULT current_timestamp;
        ',
        src_table_name
    );

    EXECUTE rb.set_updated(src_table_name);
END;
$$ LANGUAGE plpgsql;


-- Creates an associated table `*_media` for given table.
CREATE FUNCTION rb.create_media(
    src_table_name text,
    src_item_name text
)
RETURNS void
AS $$
#variable_conflict use_variable
DECLARE
    create_str text;
    pkeys rb.pkey[];
    pkeys_len integer;
    pkey_current rb.pkey;
BEGIN
    -- Get all primary keys from source table.
    pkeys := (
        SELECT
            array_agg((
                pkey_table_name,
                pkey_name,
                pkey_position,
                src_table_name,
                src_key_name,
                data_type,
                pkey_table_schema,
                src_table_schema
            )::rb.pkey)
        FROM
            rb.pkeys pk
        WHERE
            pk.pkey_table_schema = 'rb'
            AND pk.pkey_table_name = src_table_name
    );

    pkeys_len := array_length(pkeys, 1);

    -- Build create string.
    create_str := format(E'
        CREATE TABLE IF NOT EXISTS rb.%I (
            medium_id bigint NOT NULL REFERENCES rb.media(id) ON DELETE CASCADE ON UPDATE CASCADE,
            aspect_code rb.text NOT NULL REFERENCES rb.aspects(code) ON DELETE CASCADE ON UPDATE CASCADE DEFAULT \'more\',
            position integer NOT NULL DEFAULT 1 CHECK(position > 1)
        ',
        src_table_name || '_media'
    );

    FOREACH pkey_current IN ARRAY pkeys LOOP
        IF pkey_current = pkeys[1] THEN
            create_str := create_str || ',';
        END IF;

        create_str := create_str || format('
            %I %s NOT NULL REFERENCES %s.%s(%s) ON DELETE CASCADE ON UPDATE CASCADE,
            ',
            src_item_name || '_' || pkey_current.pkey_name,
            pkey_current.data_type,
            pkey_current.pkey_table_schema,
            src_table_name,
            pkey_current.pkey_name
        );
    END LOOP;

    create_str := create_str || 'PRIMARY KEY(medium_id, aspect_code, position';

    FOREACH pkey_current IN ARRAY pkeys LOOP
        create_str := create_str || ',' || src_item_name || '_' || pkey_current.pkey_name;
    END LOOP;

    create_str := create_str || '))';

    -- Create associated table *_media.
    EXECUTE create_str;
END $$
LANGUAGE plpgsql;



CREATE TYPE rb.col AS (
    name text,
    required bool,
    locale_unique bool
);

-- Creates an associated table `*_texts` for given table and columns.
CREATE FUNCTION rb.create_texts(
    src_table_name text,
    src_item_name text,
    VARIADIC cols rb.col[]
)
RETURNS void
AS $$
#variable_conflict use_variable
DECLARE
    create_str text;
    col_current rb.col;
    pkeys rb.pkey[];
    pkeys_len integer;
    pkey_current rb.pkey;
BEGIN
    -- Get all primary keys from source table.
    pkeys := (
        SELECT
            array_agg((
                pkey_table_name,
                pkey_name,
                pkey_position,
                src_table_name,
                src_key_name,
                data_type,
                pkey_table_schema,
                src_table_schema
            )::rb.pkey)
        FROM
            rb.pkeys pk
        WHERE
            pk.pkey_table_schema = 'rb'
            AND pk.pkey_table_name = src_table_name
    );

    pkeys_len := array_length(pkeys, 1);
    
    -- Build create string.
    create_str := format('
        CREATE TABLE IF NOT EXISTS rb.%I (
            translation_code rb.text NOT NULL REFERENCES rb.locales(code) ON DELETE SET NULL ON UPDATE CASCADE,
        ',
        src_table_name || '_texts'
    );

    FOREACH pkey_current IN ARRAY pkeys LOOP
        create_str := create_str || format('
            %I %s NOT NULL REFERENCES %s.%s(%s) ON DELETE CASCADE ON UPDATE CASCADE,
            ',
            src_item_name || '_' || pkey_current.pkey_name,
            pkey_current.data_type,
            pkey_current.pkey_table_schema,
            src_table_name,
            pkey_current.pkey_name
        );
    END LOOP;

    IF array_length(cols, 1) IS NOT NULL THEN
        FOREACH col_current in ARRAY cols LOOP
            create_str := create_str || col_current.name || ' text';
        
            IF col_current.required THEN
                create_str := create_str || ' NOT NULL CHECK(' || col_current.name || E' <> \'\')';
            ELSE
                create_str := create_str || ' CHECK(' || col_current.name || ' IS NULL OR ' || col_current.name || E' <> \'\')';
            END IF;

            create_str := create_str || ',';
        END LOOP;

        FOREACH col_current in ARRAY cols LOOP
            IF col_current.locale_unique THEN
                create_str := create_str || format('UNIQUE(translation_code, %I),', col_current.name);
            END IF;
        END LOOP;
    END IF;

    create_str := create_str || 'PRIMARY KEY(translation_code';

    FOREACH pkey_current IN ARRAY pkeys LOOP
        create_str := create_str || ',' || src_item_name || '_' || pkey_current.pkey_name;
    END LOOP;

    create_str := create_str || '))';

    raise notice '%', create_str;

    -- Create associated table *_media.
    EXECUTE create_str;
END $$
LANGUAGE plpgsql;


CREATE FUNCTION rb.create_texts_default(
    src_table_name text,
    src_item_name text
)
RETURNS void
AS $$
#variable_conflict use_variable
DECLARE
    col_current rb.col;
    new_table_name text;
BEGIN
    EXECUTE rb.create_texts(
        src_table_name,
        src_item_name,
        ('name', true, false),
        ('short_description', false, false),
        ('long_description', false, false)
    );

    new_table_name := src_table_name || '_texts';

    --EXECUTE rb.add_created(new_table_name);
    --EXECUTE rb.add_updated(new_table_name);
    EXECUTE rb.add_published(new_table_name);
END $$
LANGUAGE plpgsql;


--CREATE FUNCTION rb.gen_code(length integer)
--RETURNS text
--AS $$
--#variable_conflict use_variable
--DECLARE
--    chr_or_int integer;
--    code text;
--BEGIN
--    code := '';
--
--    FOR position IN 1..length LOOP
--        chr_or_int := round(random() * (2 - 1)) + 1;
--
--        CASE chr_or_int
--            WHEN 1 THEN
--                code := code || round(random() * 9)::text;
--            ELSE
--                code := code || chr((ascii('B') + round(random() * 25))::integer);
--        END CASE;
--    END LOOP;
--
--    RETURN code;
--END $$
--LANGUAGE plpgsql;

CREATE FUNCTION rb.gen_random_code(
        IN string_length integer,
        IN possible_chars TEXT DEFAULT '1234789ABEFHLMNQRTabdehr'
)
RETURNS text
AS $$
DECLARE
    output TEXT = '';
    i integer;
    pos integer;
BEGIN
    FOR i IN 1..string_length LOOP
        pos := 1 + round(random() * (length(possible_chars) - 1));
        output := output || substr(possible_chars, pos, 1);
    END LOOP;
    RETURN output;
END $$
LANGUAGE plpgsql;



-- To disallow cycles in hierarchical tables, trigger on insert and update
CREATE OR REPLACE FUNCTION rb.uncycle(src_table_name text, child_column_name text, parent_column_name text)
    RETURNS void
    LANGUAGE plpgsql
AS $$
BEGIN
    EXECUTE format(E'
        CREATE OR REPLACE FUNCTION rb.%5$s()
            RETURNS TRIGGER
            LANGUAGE plpgsql
        AS \$\$
        BEGIN
            IF EXISTS (
                WITH RECURSIVE hierarchy AS (
                    SELECT
                        NEW.%2$s, NEW.%3$s

                    UNION ALL

                    SELECT
                        current.%2$s, current.%3$s
                    FROM
                        rb.%1$s current
                    JOIN hierarchy ON
                        current.%2$s = hierarchy.%3$s
                )

                CYCLE %3$s
                SET is_cycle TO TRUE DEFAULT FALSE
                USING path

                SELECT * FROM hierarchy WHERE is_cycle IS TRUE
            )
            THEN
                RAISE EXCEPTION \'cycle detected\';
            ELSE
                RETURN NEW;
            END IF;
        END \$\$;

        CREATE CONSTRAINT TRIGGER %4$s
        AFTER INSERT OR UPDATE ON rb.%1$s
        FOR EACH ROW EXECUTE PROCEDURE rb.%5$s();
    '
    ,
        src_table_name,
        child_column_name,
        parent_column_name,
        src_table_name || '_detect_cycle',
        src_table_name || '_detect_cycle_trigger'
    );
END $$;
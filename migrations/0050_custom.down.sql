DROP INDEX products_view_idx;
DROP MATERIALIZED VIEW rb.products_view;

DROP TABLE rb.orders_products;
DROP TABLE rb.orders_statuses;
DROP TABLE rb.orders;

DROP TABLE rb.customers;

DROP TABLE rb.regions_currencies;
DROP TABLE rb.currencies;

DROP TABLE rb.products_tax_groups;
DROP TABLE rb.regions_taxes;
DROP TABLE rb.taxes_groups;
DROP TABLE rb.tax_groups_texts;
DROP TABLE rb.tax_groups;
DROP TABLE rb.taxes;

DROP TABLE rb.payment_methods_texts;
DROP TABLE rb.payment_methods;

DROP TABLE rb.bars_ratings;
DROP TABLE rb.bars;

DROP TABLE rb.products_constituents;
DROP TABLE rb.constituents_texts;
DROP TABLE rb.constituents;

DROP TABLE rb.products_sets;
DROP TABLE rb.product_sets_texts;
DROP TABLE rb.product_sets_media;
DROP TABLE rb.product_sets;
DROP TABLE rb.products_texts;
DROP TABLE rb.products_media;
DROP TABLE rb.products_prices;
DROP TABLE rb.products_values;
DROP TABLE rb.products;

DROP TABLE rb.units_texts;
DROP TABLE rb.units;
DROP TABLE rb.physical_quantities_texts;
DROP TABLE rb.physical_quantities;

DROP TABLE rb.regions_zones;
DROP TABLE rb.region_zones;
DROP TABLE rb.regions;
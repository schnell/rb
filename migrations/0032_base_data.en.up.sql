INSERT INTO rb.locales
    (code, default_locale)
VALUES
    ('en-IE', false)
;

INSERT INTO rb.slugs
    (locale_code, slug)
VALUES
    ('en-IE', 'en')
;
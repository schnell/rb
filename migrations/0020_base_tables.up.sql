-- locales
CREATE TABLE rb.locales(
    code rb.text PRIMARY KEY CHECK(code <> ''),
    default_locale boolean DEFAULT false,
    EXCLUDE (default_locale WITH =) WHERE (default_locale)
);

SELECT rb.create_texts(
    'locales', 'locale',
    ('name', true, true)
);

CREATE TABLE rb.locales_fallbacks(
    locale_code rb.text NOT NULL REFERENCES rb.locales(code) ON DELETE CASCADE ON UPDATE CASCADE,
    fallback_code rb.text NOT NULL REFERENCES rb.locales(code) ON DELETE CASCADE ON UPDATE CASCADE,
    PRIMARY KEY(locale_code, fallback_code),
    CONSTRAINT locale_and_fallback_distinct CHECK (locale_code != fallback_code)
);

SELECT rb.uncycle('locales_fallbacks', 'locale_code', 'fallback_code');

-- slugs
CREATE TABLE rb.slugs(
    slug rb.text PRIMARY KEY,
    translation_code rb.text DEFAULT NULL REFERENCES rb.locales(code) ON DELETE SET NULL ON UPDATE CASCADE,
    CONSTRAINT slug_maybe_translated UNIQUE(slug, translation_code)
);

SELECT rb.register_slugs('locales', 'locale');

CREATE TABLE rb.aspects(
    code rb.text PRIMARY KEY
);

SELECT rb.create_texts(
    'aspects', 'aspect',
    ('name', true, false),
    ('short_description', false, false)
);

-- media
CREATE TABLE rb.media(
    id bigserial PRIMARY KEY,
    path rb.text_array UNIQUE,
    hash rb.text
);

SELECT rb.register_slugs('media', 'medium');
SELECT rb.create_texts_default('media', 'medium');
SELECT rb.add_updated('media');

ALTER TABLE rb.slugs
ADD CONSTRAINT medium_unique UNIQUE(medium_id);

ALTER TABLE rb.slugs
ADD CONSTRAINT medium_forbid_translation CHECK(
    (medium_id IS NOT NULL AND translation_code IS NULL) OR
     medium_id IS     NULL
);
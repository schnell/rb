INSERT INTO rb.aspects(code)
VALUES
    ('main'  ), ('more'    ),
    ('banner'),

    ('inside'), ('outside' ),
    ('packed'), ('unpacked'),

    ('top'   ),  ('bottom' ),
    ('left'  ),  ('right'  ),
    ('front' ),  ('back'   )
;

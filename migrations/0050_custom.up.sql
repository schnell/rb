-- locales
INSERT INTO rb.locales_texts
    (name, locale_code, translation_code)
VALUES
    ('Deutsch'  , 'de-DE', 'de-DE'),
    ('German'   , 'de-DE', 'en-IE'),
    ('Englisch' , 'en-IE', 'de-DE'),
    ('English'  , 'en-IE', 'en-IE')
;

-- voc
CREATE TABLE rb.voc(
    message_code rb.text NOT NULL,
    translation_code rb.text NOT NULL REFERENCES rb.locales(code),
    message rb.text NOT NULL,

    PRIMARY KEY(message_code, translation_code)
);

-- pages
CREATE TABLE rb.pages(
    code rb.text PRIMARY KEY
);

SELECT rb.register_slugs('pages', 'page');
SELECT rb.create_texts_default('pages', 'page');

INSERT INTO rb.pages(code)
VALUES
    ('order'),
    ('legal'),
    ('subscription')
;

INSERT INTO rb.pages_texts(
    page_code, translation_code, name
)
VALUES
    ('order', 'de-DE', 'Bestellung'),
    ('legal', 'de-DE', 'Rechtliches'),
    ('subscription', 'de-DE', 'Abo')
;

INSERT INTO rb.slugs(
    slug, translation_code, page_code
)
VALUES
    ('bestellung', 'de-DE', 'order'),
    ('rechtliches', 'de-DE', 'legal'),
    ('abo', 'de-DE', 'subscription')
;

-- regions
CREATE TABLE rb.regions(
    code rb.text PRIMARY KEY
);

SELECT rb.register_slugs('regions', 'region');

INSERT INTO rb.regions(
    code
)
VALUES
    ('DE')
;

CREATE TABLE rb.region_zones(
    code rb.text PRIMARY KEY
);

INSERT INTO rb.region_zones(code)
VALUES
    ('EU'),
    ('DE')
;

CREATE TABLE rb.regions_zones(
    zone_code rb.text NOT NULL REFERENCES rb.region_zones(code) ON DELETE CASCADE ON UPDATE CASCADE,
    region_code rb.text NOT NULL REFERENCES rb.regions(code) ON DELETE CASCADE ON UPDATE CASCADE,
    PRIMARY KEY(zone_code, region_code)
);

INSERT INTO rb.regions_zones
    (zone_code, region_code)
VALUES
    ('EU', 'DE'),
    ('DE', 'DE')
;

-- physical_quantities
CREATE TABLE rb.physical_quantities(
    code rb.text PRIMARY KEY
);

INSERT INTO rb.physical_quantities(code)
VALUES
    ('quantity'),
    ('mass'),
    ('volume'),
    ('energy'),
    ('length')
;

CREATE TABLE rb.currencies(
    code rb.text PRIMARY KEY,
    symbol rb.text,
    precision integer NOT NULL CHECK(precision >= 0)
);

INSERT INTO rb.currencies(code, symbol, precision)
VALUES
    ('EUR', '€', 2)
;

CREATE TABLE rb.regions_currencies(
    region_code rb.text REFERENCES rb.regions(code) ON DELETE RESTRICT ON UPDATE CASCADE,
    currency_code rb.text NOT NULL REFERENCES rb.currencies(code) ON DELETE RESTRICT ON UPDATE CASCADE,
    PRIMARY KEY(region_code, currency_code)
);

INSERT INTO rb.regions_currencies(
    region_code, currency_code
)
VALUES
    ('DE', 'EUR')
;

SELECT rb.create_texts(
    'physical_quantities', 'physical_quantity',
    ('name', true, false)
);

INSERT INTO rb.physical_quantities_texts(
    translation_code,
    physical_quantity_code,
    name
)
VALUES
    ('de-DE'    ,'quantity'   ,'Anzahl'    ),
    ('en-IE'    ,'quantity'   ,'quantity'  ),
    ('de-DE'    ,'mass'       ,'Masse'     ),
    ('en-IE'    ,'mass'       ,'mass'      ),
    ('de-DE'    ,'volume'     ,'Volumen'   ),
    ('en-IE'    ,'volume'     ,'volume'    ),
    ('de-DE'    ,'energy'     ,'Energie'   ),
    ('en-IE'    ,'energy'     ,'energy'    ),
    ('de-DE'    ,'length'     ,'Länge'     ),
    ('en-IE'    ,'length'     ,'length'    )
;

-- units
CREATE TABLE rb.units(
    code rb.text PRIMARY KEY,
    symbol varchar NOT NULL UNIQUE,
    physical_quantity_code rb.text NOT NULL REFERENCES rb.physical_quantities(code) ON DELETE RESTRICT ON UPDATE CASCADE,
    metrically_prefixable boolean NOT NULL DEFAULT TRUE
);

INSERT INTO rb.units(physical_quantity_code, code, symbol)
VALUES
    ('quantity' , 'quantity'    , ''),
    ('mass'     , 'gram'        , 'g'),
    ('volume'   , 'liter'       , 'l'),
    ('energy'   , 'joule'       , 'J'),
    ('length'   , 'meter'       , 'm')
;

SELECT rb.create_texts(
    'units', 'unit',
    ('one', true, false),
    ('few', false, false),
    ('many', true, false)
);

INSERT INTO rb.units_texts(
    translation_code,
    unit_code,
    one,
    many
)
VALUES
    ('de-DE', 'gram'    , 'Gramm'   , 'Gramm'   ),
    ('en-IE', 'gram'    , 'gram'    , 'grams'   ),
    ('de-DE', 'liter'   , 'Liter'   , 'Liter'   ),
    ('en-IE', 'liter'   , 'litre'   , 'litres'  ),
    ('de-DE', 'joule'   , 'Joule'   , 'Joule'   ),
    ('en-IE', 'joule'   , 'joule'   , 'joules'  ),
    ('de-DE', 'meter'   , 'Meter'   , 'Meter'   ),
    ('en-IE', 'meter'   , 'metre'   , 'metres'  )
;

-- sessions
CREATE TABLE rb.shop_sessions(
    id varchar NOT NULL PRIMARY KEY,
    expires timestamp NULL,
    session jsonb NOT NULL
);

CREATE TABLE rb.editor_sessions(
    id varchar NOT NULL PRIMARY KEY,
    expires timestamp NULL,
    session jsonb NOT NULL
);

-- editors
CREATE TABLE rb.editors(
    name rb.text PRIMARY KEY,
    call_name rb.text,
    email rb.email,

    password_hash rb.text CHECK(password_hash IS NULL OR length(password_hash) > 10),
    password_reset_code rb.text,
    password_reset_expires timestamp,

    data_expires timestamp,

    preferred_locale rb.text REFERENCES rb.locales(code) ON DELETE SET NULL ON UPDATE CASCADE
);

CREATE TABLE rb.editors_sessions(
    editor_name rb.text NOT NULL REFERENCES rb.editors(name) ON DELETE CASCADE ON UPDATE CASCADE,
    session_id varchar NOT NULL REFERENCES rb.editor_sessions(id) ON DELETE CASCADE ON UPDATE CASCADE,
    PRIMARY KEY(editor_name, session_id)
);

-- customers
CREATE TABLE rb.customers(
    id bigserial PRIMARY KEY,
    call_name rb.text,
    email rb.email,

    password_hash rb.text CHECK(password_hash IS NULL OR length(password_hash) > 10),
    password_reset_code rb.text,
    password_reset_expires timestamp,

    data_expires timestamp,

    preferred_locale rb.text REFERENCES rb.locales(code) ON DELETE SET NULL ON UPDATE CASCADE,

    invoice_region_code rb.text REFERENCES rb.regions(code) ON DELETE SET NULL ON UPDATE CASCADE,
    invoice_postal_address rb.postal_address,

    shipping_region_code rb.text REFERENCES rb.regions(code) ON DELETE SET NULL ON UPDATE CASCADE,
    shipping_postal_address rb.postal_address
);

CREATE TABLE rb.customers_sessions(
    customer_id bigint NOT NULL REFERENCES rb.customers(id) ON DELETE CASCADE ON UPDATE CASCADE,
    session_id varchar NOT NULL REFERENCES rb.shop_sessions(id) ON DELETE CASCADE ON UPDATE CASCADE,
    PRIMARY KEY(customer_id, session_id)
);

-- producers
CREATE TABLE rb.producers(
    code rb.text PRIMARY KEY,
    region_code rb.text REFERENCES rb.regions(code) ON DELETE SET NULL ON UPDATE CASCADE,
    postal_address rb.postal_address,
    coordinates point
);

SELECT rb.create_texts_default('producers', 'producer');
SELECT rb.register_slugs('producers', 'producer');

-- products
CREATE TABLE rb.products(
    code rb.text PRIMARY KEY,
    base_code rb.text REFERENCES rb.products(code) ON DELETE RESTRICT ON UPDATE CASCADE CHECK(code != base_code),

    unit_code rb.text REFERENCES rb.units(code) ON DELETE CASCADE ON UPDATE CASCADE DEFAULT 'quantity',
    base_value rb.text DEFAULT 1,

    CONSTRAINT unit_from_base_product CHECK(
        (base_code IS     NULL AND unit_code IS NOT NULL) OR
        (base_code IS NOT NULL AND unit_code IS     NULL)
    )
);

SELECT rb.uncycle('products', 'code', 'base_code');
SELECT rb.add_published('products');
SELECT rb.create_texts_default('products', 'product');
SELECT rb.create_media('products', 'product');
SELECT rb.register_slugs('products', 'product');

-- TODO: create trigger on update/insert for products
CREATE TABLE rb.products_history(
    current_code rb.text REFERENCES rb.products(code) ON DELETE SET NULL ON UPDATE CASCADE,

    history_code rb.text NOT NULL,
    updated_at timestamp NOT NULL DEFAULT now(),

    history_base_code rb.text,
    history_base_updated_at timestamp,

    ean rb.text,

    unit_code rb.text REFERENCES rb.units(code) ON DELETE RESTRICT ON UPDATE CASCADE,
    base_value numeric,

    FOREIGN KEY(history_base_code, history_base_updated_at)
        REFERENCES rb.products_history(history_code, updated_at) ON DELETE NO ACTION ON UPDATE CASCADE,

    PRIMARY KEY(history_code, updated_at)
);

-- TODO: create trigger on update/insert for products_texts
CREATE TABLE rb.products_texts_history(
    translation_code rb.text REFERENCES rb.locales(code) ON DELETE RESTRICT ON UPDATE CASCADE,

    product_history_code rb.text NOT NULL,
    product_history_updated_at timestamp NOT NULL,

    updated_at timestamp NOT NULL DEFAULT now(),

    name rb.text NOT NULL,
    short_description rb.text,
    long_description rb.text,

    FOREIGN KEY(product_history_code, product_history_updated_at)
        REFERENCES rb.products_history(history_code, updated_at) ON DELETE RESTRICT ON UPDATE CASCADE,

    PRIMARY KEY(product_history_code, product_history_updated_at, translation_code, updated_at)
);

CREATE TABLE rb.products_producers(
    product_code rb.text NOT NULL REFERENCES rb.products(code) ON DELETE CASCADE ON UPDATE CASCADE,
    producer_code rb.text NOT NULL REFERENCES rb.producers(code) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE rb.products_prices(
    product_code rb.text NOT NULL REFERENCES rb.products(code) ON DELETE CASCADE ON UPDATE CASCADE,
    region_code rb.text NOT NULL REFERENCES rb.regions(code) ON DELETE CASCADE ON UPDATE CASCADE,

    value_from rb.text NOT NULL,
    price_gross numeric NOT NULL,

    PRIMARY KEY(product_code, region_code, value_from)
);

-- TODO: create trigger on update/insert for products_prices
CREATE TABLE rb.products_prices_history(
    region_code rb.text NOT NULL REFERENCES rb.regions(code) ON DELETE SET NULL ON UPDATE CASCADE,

    product_history_code rb.text NOT NULL,
    product_history_updated_at timestamp NOT NULL,

    updated_at timestamp NOT NULL DEFAULT now(),

    value_from rb.text NOT NULL,
    price_gross numeric NOT NULL,

    FOREIGN KEY(product_history_code, product_history_updated_at)
        REFERENCES rb.products_history(history_code, updated_at) ON DELETE RESTRICT ON UPDATE CASCADE,

    PRIMARY KEY(product_history_code, product_history_updated_at, region_code, updated_at)
);

SELECT rb.set_updated('products_prices');

-- stocks
CREATE TABLE rb.stocks(
    code rb.text PRIMARY KEY,
    region_code rb.text REFERENCES rb.regions(code) ON DELETE RESTRICT ON UPDATE CASCADE,
    postal_address rb.postal_address,
    coordinates point
);

CREATE TABLE rb.products_stocks(
    product_code rb.text NOT NULL REFERENCES rb.products(code) ON DELETE RESTRICT ON UPDATE CASCADE,
    stock_code rb.text NOT NULL REFERENCES rb.stocks(code) ON DELETE RESTRICT ON UPDATE CASCADE,
    updated_at timestamp NOT NULL DEFAULT now(),

    quantity numeric NOT NULL,

    PRIMARY KEY(product_code, stock_code, updated_at)
);

SELECT rb.set_updated('products_stocks');

-- product_groups
CREATE TABLE rb.product_groups(
    code rb.text PRIMARY KEY,
    base_code rb.text REFERENCES rb.product_groups(code) ON DELETE CASCADE ON UPDATE CASCADE
);

SELECT rb.uncycle('product_groups', 'code', 'base_code');
SELECT rb.add_published('product_groups');
SELECT rb.create_texts_default('product_groups', 'product_group');
SELECT rb.register_slugs('product_groups', 'product_group');
SELECT rb.create_media('product_groups', 'product_group');

-- products_groups
CREATE TABLE rb.products_groups(
    product_code rb.text NOT NULL REFERENCES rb.products(code) ON DELETE CASCADE ON UPDATE CASCADE,
    group_code rb.text NOT NULL REFERENCES rb.product_groups(code) ON DELETE CASCADE ON UPDATE CASCADE,

    PRIMARY KEY(product_code, group_code)
);

INSERT INTO rb.product_groups(
    code, base_code
)
VALUES
    ('foods', NULL),
    ('bars' , 'foods')
;

INSERT INTO rb.product_groups_texts(
    translation_code, product_group_code, name
)
VALUES
    ('de-DE', 'foods'   , 'Lebensmittel'),
    ('de-DE', 'bars'    , 'Riegel')
;

INSERT INTO rb.slugs(
    translation_code, product_group_code, slug
)
VALUES
    ('de-DE', 'foods'   , 'lebensmittel'),
    ('de-DE', 'bars'    , 'riegel')
;

-- product_properties, e.g. nutritional_values
CREATE TABLE rb.properties(
    code rb.text PRIMARY KEY,
    base_code rb.text REFERENCES rb.properties(code) ON DELETE CASCADE ON UPDATE CASCADE
);

SELECT rb.uncycle('properties', 'code', 'base_code');
SELECT rb.register_slugs('properties', 'property');
SELECT rb.create_texts_default('properties', 'property');

INSERT INTO rb.properties(
    code, base_code
)
VALUES
    ('energy'           , NULL      ),
    ('protein'          , NULL      ),
    ('fats'             , NULL      ),
    ('fats-unsaturated' , 'fats'    ),
    ('fats-saturated'   , 'fats'    ),
    ('carbs'            , NULL      ),
    ('sugars'           , 'carbs'   ),
    ('polyols'          , 'carbs'   ),
    ('fibers'           , NULL      ),
    ('salt'             , NULL      ),
    ('width'            , NULL      ),
    ('height'           , NULL      ),
    ('length'           , NULL      ),
    ('weight'           , NULL      ),
    ('volume'           , NULL      )
;

-- property_groups
CREATE TABLE rb.property_groups(
    code rb.text PRIMARY KEY,
    base_code rb.text REFERENCES rb.property_groups(code) ON DELETE CASCADE ON UPDATE CASCADE
);

SELECT rb.uncycle('property_groups', 'code', 'base_code');
SELECT rb.create_texts_default('property_groups', 'property_group');

CREATE TABLE rb.properties_groups(
    group_code rb.text NOT NULL REFERENCES rb.property_groups(code) ON DELETE CASCADE ON UPDATE CASCADE,
    property_code rb.text NOT NULL REFERENCES rb.properties(code) ON DELETE CASCADE ON UPDATE CASCADE,
    PRIMARY KEY(group_code, property_code)
);

INSERT INTO rb.property_groups(
    code
)
VALUES
    ('nutritional'),
    ('geometry')
;

INSERT INTO rb.property_groups_texts(
    translation_code, property_group_code, name
)
VALUES
    ('de-DE', 'nutritional', 'Nährwerte'),
    ('de-DE', 'geometry', 'Geometrie')
;

INSERT INTO rb.properties_groups(
    group_code, property_code
)
VALUES
    ('nutritional'  , 'energy'           ),
    ('nutritional'  , 'protein'          ),
    ('nutritional'  , 'fats-unsaturated' ),
    ('nutritional'  , 'fats-saturated'   ),
    ('nutritional'  , 'carbs'            ),
    ('nutritional'  , 'sugars'           ),
    ('nutritional'  , 'polyols'          ),
    ('nutritional'  , 'fibers'           ),
    ('nutritional'  , 'salt'             ),
    ('geometry'     , 'width'            ),
    ('geometry'     , 'height'           ),
    ('geometry'     , 'length'           )
;

CREATE TABLE rb.properties_units(
    property_code rb.text NOT NULL REFERENCES rb.properties(code) ON DELETE RESTRICT ON UPDATE CASCADE,
    unit_code rb.text NOT NULL REFERENCES rb.units(code) ON DELETE RESTRICT ON UPDATE CASCADE,

    PRIMARY KEY(property_code, unit_code)
);

INSERT INTO rb.properties_units(
    property_code, unit_code
)
VALUES
    ('energy'           , 'joule'       ),

    ('protein'          , 'gram'        ),
    ('fats'             , 'gram'        ),
    ('fats-unsaturated' , 'gram'        ),
    ('fats-saturated'   , 'gram'        ),
    ('carbs'            , 'gram'        ),
    ('sugars'           , 'gram'        ),
    ('polyols'          , 'gram'        ),
    ('fibers'           , 'gram'        ),
    ('salt'             , 'gram'        ),

    ('fats'             , 'liter'       ),
    ('fats-unsaturated' , 'liter'       ),
    ('fats-saturated'   , 'liter'       ),

    ('width'            , 'meter'       ),
    ('height'           , 'meter'       ),
    ('length'           , 'meter'       ),

    ('weight'           , 'gram'        ),
    ('volume'           , 'liter'       )
;


-- per product_unit
CREATE TABLE rb.products_properties(
    product_code rb.text NOT NULL REFERENCES rb.products(code) ON DELETE CASCADE ON UPDATE CASCADE,

    property_code rb.text NOT NULL,
    unit_code rb.text NOT NULL,

    value rb.text NOT NULL,

    FOREIGN KEY(property_code, unit_code)
        REFERENCES rb.properties_units(property_code, unit_code) ON DELETE RESTRICT ON UPDATE CASCADE,
    
    PRIMARY KEY(product_code, property_code)
);

SELECT rb.set_updated('products_properties');

-- ratings
CREATE TABLE rb.rating_parameters(
    code rb.text PRIMARY KEY,
    base_code rb.text REFERENCES rb.rating_parameters(code) ON DELETE CASCADE ON UPDATE CASCADE,
    steps integer NOT NULL CHECK(steps >= 2)
);

SELECT rb.uncycle('rating_parameters', 'code', 'base_code');
SELECT rb.register_slugs('rating_parameters', 'rating_parameter');

INSERT INTO rb.rating_parameters(
    code, steps
)
VALUES
    ('crispiness'   , 5),
    ('mouthcoating' , 5),
    ('smoothness'   , 5),
    ('granularity'  , 5),
    ('chewiness'    , 5),
    ('sweetness'    , 5),
    ('gumminess'    , 5),
    ('hardness'     , 5),
    ('uniformity'   , 5),
    ('bounce'       , 5),
    ('dryness'      , 5),
    ('adhesiveness' , 5)
;

CREATE TABLE rb.ratings(
    id bigserial PRIMARY KEY,
    code rb.text UNIQUE DEFAULT gen_random_code(25),
    customer_id bigint UNIQUE REFERENCES rb.customers(id) ON DELETE SET NULL ON UPDATE CASCADE
);

CREATE TABLE rb.products_ratings(
    rating_id integer NOT NULL REFERENCES rb.ratings(id) ON DELETE CASCADE ON UPDATE CASCADE,
    product_code rb.text NOT NULL REFERENCES rb.products(code) ON DELETE CASCADE ON UPDATE CASCADE,
    rating_parameter_code rb.text NOT NULL REFERENCES rb.rating_parameters(code) ON DELETE CASCADE ON UPDATE CASCADE,

    value integer NOT NULL CHECK(value >= 1),

    PRIMARY KEY(rating_id, product_code, rating_parameter_code)
);

SELECT rb.add_created('products_ratings');
SELECT rb.add_updated('products_ratings');

-- notes
CREATE TABLE rb.notes(
    code rb.text PRIMARY KEY,
    symbol rb.text NOT NULL UNIQUE
);

SELECT rb.create_texts_default('notes', 'note');

INSERT INTO rb.notes(
    code, symbol
)
VALUES
    ('organic-eu', 'bio'),
    ('allergen', 'a')
;

INSERT INTO rb.notes_texts(
    translation_code, note_code, name
)
VALUES
    ('de-DE', 'organic-eu', 'EU-Bio')
;

CREATE TABLE rb.products_notes(
    note_code rb.text NOT NULL REFERENCES rb.notes(code) ON DELETE CASCADE ON UPDATE CASCADE,
    product_code rb.text NOT NULL REFERENCES rb.products(code) ON DELETE CASCADE ON UPDATE CASCADE,
    PRIMARY KEY(note_code, product_code)
);

-- constituents
CREATE TABLE rb.constituents(
    id bigserial PRIMARY KEY
);

SELECT rb.create_texts_default('constituents', 'constituent');
SELECT rb.register_slugs('constituents', 'constituent');

CREATE TABLE rb.products_constituents(
    product_code rb.text NOT NULL REFERENCES rb.products(code) ON DELETE CASCADE ON UPDATE CASCADE,
    constituent_id bigint NOT NULL REFERENCES rb.constituents(id) ON DELETE RESTRICT ON UPDATE CASCADE,
    position bigint NOT NULL DEFAULT 1,
    depth bigint NOT NULL DEFAULT 1,

    PRIMARY KEY(product_code, constituent_id, position, depth)
);



CREATE TABLE rb.products_constituents_notes(
    product_code rb.text NOT NULL,
    constituent_id bigint NOT NULL,
    position bigint NOT NULL,
    depth bigint NOT NULL,

    note_code rb.text NOT NULL REFERENCES rb.notes(code) ON DELETE CASCADE ON UPDATE CASCADE,
    
    FOREIGN KEY(product_code, constituent_id, position, depth)
        REFERENCES rb.products_constituents(product_code, constituent_id, position, depth) ON DELETE CASCADE ON UPDATE CASCADE,
    PRIMARY KEY(product_code, constituent_id, position, depth, note_code)
);

CREATE TABLE rb.products_constituents_properties(
    product_code rb.text NOT NULL,
    constituent_id bigint NOT NULL,
    position bigint NOT NULL,
    depth bigint NOT NULL,

    property_code rb.text NOT NULL REFERENCES rb.properties(code) ON DELETE CASCADE ON UPDATE CASCADE,
    value rb.text NOT NULL,
    
    FOREIGN KEY(product_code, constituent_id, position, depth)
        REFERENCES rb.products_constituents(product_code, constituent_id, position, depth) ON DELETE CASCADE ON UPDATE CASCADE,
    PRIMARY KEY(product_code, constituent_id, position, depth, property_code)
);

-- payment_methods
CREATE TABLE rb.payment_methods(
    code rb.text PRIMARY KEY CHECK(code <> '')
);

SELECT rb.create_texts_default('payment_methods', 'payment_method');

-- shipping_methods
CREATE TABLE rb.shipping_methods(
    code rb.text PRIMARY KEY CHECK(code <> ''),
    price_calculation_rule rb.text NOT NULL
);

SELECT rb.create_texts_default('shipping_methods', 'shipping_method');

-- taxes
CREATE TABLE rb.taxes(
    code rb.text PRIMARY KEY CHECK(code <> ''),
    region_code rb.text NOT NULL REFERENCES rb.region_zones(code) ON DELETE RESTRICT ON UPDATE CASCADE
);

SELECT rb.create_texts(
    'taxes'     , 'tax'     ,
    ('name'     , false     , false)
);

INSERT INTO rb.taxes(
    region_code, code
)
VALUES
    ('DE'       , 'vat-de'          ),
    ('DE'       , 'vat-de-reduced'  )
;

INSERT INTO rb.taxes_texts(
    translation_code, tax_code, name
)
VALUES
    ('de-DE'    , 'vat-de'          , 'Umsatzsteuer'            ),
    ('de-DE'    , 'vat-de-reduced'  , 'ermäßigte Umsatzsteuer'  )
;

CREATE TABLE rb.taxes_rates(
    tax_code rb.text NOT NULL REFERENCES rb.taxes(code) ON DELETE RESTRICT ON UPDATE CASCADE,
    rate numeric NOT NULL,
    updated_at timestamp NOT NULL DEFAULT now(),

    PRIMARY KEY(tax_code, updated_at)
);

SELECT rb.set_updated('taxes_rates');

INSERT INTO rb.taxes_rates(
    tax_code, rate
)
VALUES
    ('vat-de'           , 0.07),
    ('vat-de-reduced'   , 0.19)
;

CREATE TABLE rb.products_taxes(
    product_code rb.text NOT NULL REFERENCES rb.products(code) ON DELETE RESTRICT ON UPDATE CASCADE,
    tax_code rb.text NOT NULL REFERENCES rb.taxes(code) ON DELETE RESTRICT ON UPDATE CASCADE,
    PRIMARY KEY(product_code, tax_code)
);

CREATE TABLE rb.orders(
    code rb.text PRIMARY KEY DEFAULT gen_random_code(25),
    customer_id bigint NOT NULL REFERENCES rb.customers(id) ON DELETE RESTRICT ON UPDATE CASCADE
);

SELECT rb.add_created('orders');

CREATE TABLE rb.orders_statuses(
    -- id
    order_code rb.text NOT NULL REFERENCES rb.orders(code) ON DELETE RESTRICT ON UPDATE CASCADE,
    updated_at TIMESTAMP NOT NULL DEFAULT now(),

    -- status
    ordered             boolean,
    priced              boolean,
    cancelled           boolean,
    items_changed       boolean,
    price_changed       boolean,
    invoiced            boolean,
    invoice_overdue     boolean,
    paid_fully          boolean,
    paid_partially      boolean,
    paid_over           boolean,
    items_shipped       boolean,
    items_arrived       boolean,
    items_returned      boolean,
    inquiry_received    boolean,
    inquiry_accepted    boolean,
    inquiry_rejected    boolean,
    claim_received      boolean,
    claim_accepted      boolean,
    claim_rejected      boolean,
    items_repaired      boolean,
    items_replaced      boolean,
    refunded_fully      boolean,
    refunded_partially  boolean,
    refunded_over       boolean,
    thanked             boolean,
    closed              boolean,
    reopened            boolean,

    -- message
    message rb.text,
    message_translation_code rb.text REFERENCES rb.locales(code) ON DELETE NO ACTION ON UPDATE CASCADE,

    -- by/from
    by_region rb.text REFERENCES rb.regions(code) ON DELETE NO ACTION ON UPDATE CASCADE,
    by_customer boolean NOT NULL DEFAULT FALSE,
    by_stock rb.text REFERENCES rb.stocks(code) ON DELETE NO ACTION ON UPDATE CASCADE,
    by_us boolean NOT NULL DEFAULT FALSE,

    -- to
    to_region rb.text REFERENCES rb.regions(code) ON DELETE NO ACTION ON UPDATE CASCADE,
    to_customer boolean NOT NULL DEFAULT FALSE,
    to_stock rb.text REFERENCES rb.stocks(code) ON DELETE NO ACTION ON UPDATE CASCADE,
    to_us boolean,

    -- at
    at_region rb.text REFERENCES rb.regions(code) ON DELETE NO ACTION ON UPDATE CASCADE,
    at_customer boolean NOT NULL DEFAULT FALSE,
    at_stock rb.text REFERENCES rb.stocks(code) ON DELETE NO ACTION ON UPDATE CASCADE,
    at_us boolean NOT NULL DEFAULT FALSE,

    -- price
    price_currency_code rb.text REFERENCES rb.currencies(code) ON DELETE NO ACTION ON UPDATE CASCADE,
    price_gross numeric,
    price_net numeric,
    price_shipping numeric,
    price_taxes numeric,

    -- payment
    payment_currency_code rb.text REFERENCES rb.currencies(code) ON DELETE RESTRICT ON UPDATE CASCADE,
    payment_sum numeric,
    payment_method_code rb.text REFERENCES rb.payment_methods(code) ON DELETE NO ACTION ON UPDATE CASCADE,
    payment_code rb.text,

    -- shipping
    shipping_method_code rb.text REFERENCES rb.shipping_methods(code) ON DELETE NO ACTION ON UPDATE CASCADE,
    shipping_code rb.text,
    shipping_postal_address rb.postal_address,

    -- primary key
    PRIMARY KEY(order_code, updated_at),

    -- at_least_one_status
    CONSTRAINT at_least_one_status CHECK(
        ordered             IS NOT NULL OR
        priced              IS NOT NULL OR
        cancelled           IS NOT NULL OR
        items_changed       IS NOT NULL OR
        price_changed       IS NOT NULL OR
        invoiced            IS NOT NULL OR
        invoice_overdue     IS NOT NULL OR
        paid_fully          IS NOT NULL OR
        paid_partially      IS NOT NULL OR
        paid_over           IS NOT NULL OR
        items_shipped       IS NOT NULL OR
        items_arrived       IS NOT NULL OR
        items_returned      IS NOT NULL OR
        inquiry_received    IS NOT NULL OR
        inquiry_accepted    IS NOT NULL OR
        inquiry_rejected    IS NOT NULL OR
        claim_received      IS NOT NULL OR
        claim_accepted      IS NOT NULL OR
        claim_rejected      IS NOT NULL OR
        items_repaired      IS NOT NULL OR
        items_replaced      IS NOT NULL OR
        refunded_fully      IS NOT NULL OR
        refunded_partially  IS NOT NULL OR
        refunded_over       IS NOT NULL OR
        thanked             IS NOT NULL OR
        closed              IS NOT NULL OR
        reopened            IS NOT NULL
    )
);

SELECT rb.set_updated('orders_statuses');

CREATE TABLE rb.orders_products(
    order_code rb.text NOT NULL REFERENCES rb.orders(code) ON DELETE RESTRICT ON UPDATE CASCADE,

    set_id integer NOT NULL CHECK(set_id >= 1),

    product_code rb.text NOT NULL,
    product_updated_at timestamp NOT NULL,

    value numeric NOT NULL,
    price_gross numeric NOT NULL,

    FOREIGN KEY(product_code, product_updated_at)
        REFERENCES rb.products_history(history_code, updated_at) ON DELETE RESTRICT ON UPDATE CASCADE,

    PRIMARY KEY(
        order_code,
        set_id,
        product_code, product_updated_at
    ),
    
    CONSTRAINT unique_set_and_product UNIQUE(order_code, set_id, product_code)
);


----DROP INDEX products_view_idx;
----DROP MATERIALIZED VIEW IF EXISTS rb.products_view;
--CREATE MATERIALIZED VIEW rb.products_view(
--    -- order by
--    arg_locales.code translation_code,
--    arg_regions.code region_code,
--
--    -- ids
--    name,
--    code,
--    ean,
--    slugs,
--
--    -- published
--    published,
--    published_at,
--
--    -- groups
--    groups,
--
--    -- texts
--    short_description, long_description,
--
--    -- media
--    media,
--
--    -- prices, taxes
--    prices
----  taxes
--
--    -- weight, volume
----    weight,
----    volume,
--    -- constituents
----    constituents,
--    -- nutrition
----    nutrition,
--    -- geometry
----    geometry,
--    -- rating
----    rating
--) AS
--WITH arg_regions AS(
--    SELECT
--        arg_region.code
--    FROM
--        regions arg_region
--)
--WITH arg_locales AS(
--    SELECT
--        arg_locale.code
--    FROM
--        regions arg_region
--)
--SELECT
--    arg_regions.code,
--    arg_locales.code,
--    name,
--    products.code as arg_product.code,
--
--    (
--        SELECT json_agg(json_build_object(
--            'slug', slug,
--            'translation_code', translation_code
--        ))
--        FROM
--            slugs
--        WHERE
--            product_code = products.code
--    ) AS
--    slugs,
--
--    products.published,
--    products.published_at,
--
--    short_description,
--    long_description,
--
--    (
--        SELECT json_agg(json_build_object(
--            'code', products_groups.group_code,
--            'name', name,
--            'slugs', (
--                SELECT json_agg(json_build_object(
--                    'slug', slug,
--                    'translation_code', translation_code
--                ))
--                FROM
--                    slugs
--                WHERE
--                    product_group_code = products_groups.group_code
--            )
--        ))
--        FROM
--            products_groups
--        LEFT JOIN
--            product_groups_texts texts
--        ON
--            product_group_code = products_groups.group_code
--        WHERE
--            product_code = products.code AND
--            texts.translation_code = translation_code AND
--            published = TRUE
--    ) AS
--    groups,
--
--    (
--        SELECT json_agg(json_build_object(
--            'id', products_media.medium_id,
--            'name', name,
--            'slugs', (
--                SELECT json_agg(json_build_object(
--                    'slug', slug,
--                    'translation_code', translation_code
--                ))
--                FROM
--                    slugs
--                WHERE
--                    slugs.medium_id = products_media.medium_id
--            ),
--            'width', width,
--            'height', height,
--            'depth', depth,
--            'mime_type', (mime_type || '/' || mime_sub_type)
--        ))
--        FROM
--            products_media
--        LEFT JOIN
--            media_texts texts
--        ON
--            texts.medium_id = products_media.medium_id
--        LEFT JOIN
--            media
--        ON
--            media.id = products_media.medium_id
--        WHERE
--            product_code = products.code AND
--            texts.translation_code = arg_locales.code AND
--            published = TRUE
--    ) AS
--    media,
--
--    (
--        SELECT
--            json_agg(json_build_object(
--                'value_from', value_from,
--                'price_gross', price_gross,
--                'region_zone_code', region_zone_code
--        )) AS prices
--        FROM (
--            prices
--        WHERE
--            prices.product_code = products.code AND
--            region_zone_code = any(
--                SELECT
--                    array_agg(group_code)
--                FROM
--                    regions_zones
--                WHERE
--                    region_code = args_regions.code
--            )
--        -- TODO: resolve region_code from region_zone_code
--    ) AS
--    prices,
--
--    ean
--FROM
--    products arg_product
--LEFT JOIN
--    products_texts
--ON
--    texts.product_code = arg_product.code
--LEFT JOIN
--    locales
--ON
--    locales.code = texts.translation_code
--WHERE
--    texts.published = TRUE
--GROUP BY(
--    arg_locales.code,
--    arg_regions.code
--)
--;
--
--
--
--
----CREATE UNIQUE INDEX products_view_idx ON rb.products_view(code, region_code, translation_code);
----REFRESH MATERIALIZED VIEW CONCURRENTLY rb.products_view;
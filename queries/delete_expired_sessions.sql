DELETE FROM
    editors_sessions,
    customers_sessions
WHERE
    expires <= now()
;
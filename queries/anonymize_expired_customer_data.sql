WITH
    expired_customers(id)
    AS (
        UPDATE 
            customers
        SET
            call_name                   = NULL,
            email                       = NULL,
            password_hash               = NULL,
            password_reset_code         = NULL,
            password_reset_expires      = NULL,
            data_expires                = NULL,
            preferred_locale            = NULL,
            invoice_region_code         = NULL,
            invoice_postal_address      = NULL,
            shipping_region_code        = NULL,
            shipping_postal_address     = NULL
        WHERE
            data_expires <= now()
        RETURNING
            id
    )
UPDATE
    ratings
SET
    customer_id         = NULL,
    code                = NULL
FROM
    expired_customers
WHERE
    customer_id         = expired_customers.id
;
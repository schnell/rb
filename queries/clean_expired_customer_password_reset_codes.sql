UPDATE
    customers
SET
    password_reset_code = NULL,
    password_reset_expires = NULL
WHERE
    password_reset_expires <= now()
;
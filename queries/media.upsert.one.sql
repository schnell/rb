INSERT INTO
    media(
        path                ,
        hash                ,
        updated_at          ,
        mime_main_type      ,
        mime_sub_type       ,
        width               ,	
        height              ,
        depth                   
    )
VALUES
    (
        $1,
        $2,
        $3,
        $4,
        $5,
        $6,
        $7,
        $8
    )
ON CONFLICT (path) DO UPDATE SET
    hash                    = EXCLUDED.hash             ,
    updated_at              = EXCLUDED.updated_at       ,
    mime_main_type          = EXCLUDED.mime_main_type   ,
    mime_sub_type           = EXCLUDED.mime_sub_type    ,
    width                   = EXCLUDED.width            ,	
    height                  = EXCLUDED.height           ,
    depth                   = EXCLUDED.depth
RETURNING
    *
;
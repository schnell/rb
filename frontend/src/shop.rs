//! Shop endpoint created by [app] and various handler functions.

use crate::{
    config::{BaseUrl, HttpsConfig},
    media::{MediaResizeSteps, MediumId, Medium, MediumItem},
    repo::Repo,
    slug::SlugItem,
    Error,
};

use poem::{
    get, handler,
    http::{
        header::{CACHE_CONTROL, CONTENT_TYPE, LAST_MODIFIED},
        HeaderMap, HeaderValue, StatusCode,
    },
    middleware::AddData,
    middleware::{AddDataEndpoint, CookieJarManagerEndpoint},
    session::{CookieConfig, ServerSession, ServerSessionEndpoint, Session},
    web::{cookie::SameSite, Data, IntoResponse, Path, Redirect},
    EndpointExt, Request, Response, Route,
};
use poem_dbsession::{sqlx::PgSessionStorage, DatabaseConfig};
use rand::random;
use serde::Deserialize;
use std::{sync::Arc, time::Duration};
use tera::Tera;

/// Endpoint type returned from [app].
pub type ShopEndpoint = AddDataEndpoint<
    AddDataEndpoint<
        CookieJarManagerEndpoint<
            ServerSessionEndpoint<
                PgSessionStorage,
                AddDataEndpoint<AddDataEndpoint<Route, Arc<Tera>>, Arc<Repo>>,
            >,
        >,
        Arc<BaseUrl>,
    >,
    Arc<MediaResizeSteps>,
>;

/// Creates an endpoint for the shop with given configuration and dependencies.
pub async fn app(
    templates_glob: impl Into<String>,
    https_config: HttpsConfig,
    repo: Arc<Repo>,
    media_resize_steps: MediaResizeSteps,
) -> Result<ShopEndpoint, Error> {
    let templates_glob = templates_glob.into();
    let templates = Arc::new(Tera::new(&templates_glob)?);
    let base_url = Arc::new(https_config.base_url());
    let media_resize_steps = Arc::new(media_resize_steps);

    let session_storage = PgSessionStorage::try_new(
        DatabaseConfig::new().table_name("shop_sessions"),
        (*repo.pg_pool).clone(),
    )
    .await
    .unwrap();

    let app = Route::new()
        .at("/", get(index).post(index))
        .at("/:slug", get(find).post(find))
        .with(AddData::new(templates.clone()))
        .with(AddData::new(repo.clone()))
        .with(ServerSession::new(
            CookieConfig::new()
                .name("session")
                .secure(true)
                .domain(https_config.host())
                .same_site(SameSite::Strict)
                .max_age(Duration::from_secs(2 * 60 * 60)),
            session_storage,
        ))
        .with(AddData::new(base_url))
        .with(AddData::new(media_resize_steps));

    Ok(app)
}

/// Handles request for `/`.
#[handler]
async fn index(
    _req: &Request,
    _session: &Session,
    templates: Data<&Arc<Tera>>,
    _repo: Data<&Arc<Repo>>,
) -> Response {
    let context = tera::Context::new();
    let body = templates.render("index.html", &context);
    let mut headers = HeaderMap::new();
    headers.insert(CONTENT_TYPE, HeaderValue::from_static("text/html"));

    match body {
        Ok(body) => (StatusCode::OK, headers, body).into_response(),
        Err(error) => {
            return (
                StatusCode::INTERNAL_SERVER_ERROR,
                headers,
                format!("<pre>{:#?}</pre>", error),
            )
                .into_response();
        }
    }
}

#[derive(Deserialize)]
struct ShopFindMedia {
    pub width: Option<u32>,
    pub height: Option<u32>,
}

#[inline]
async fn media_ok_or_redirect(
    req: &Request,
    base_url: &Arc<BaseUrl>,
    slug: String,
    medium: Arc<Medium>,
    media_resize_steps: &Arc<MediaResizeSteps>,
) -> Response {
    if medium.data.is_none() {
        return redirect_to_index(base_url);
    }
    
    enum To {
        Width(u32),
        Height(u32),
        Original,
    }

    let redirect_to = |to: To| {
        let mut url: String = (**base_url).clone().0 + &slug;
        
        match to {
            To::Width(width) => url += &format!("?width={width}"),
            To::Height(height) => url += &format!("?height={height}"),
            To::Original => {},
        }

        Redirect::see_other(url).into_response()
    };

    let mut data = None;

    if let Ok(query) = req.params::<ShopFindMedia>() {
        match (query.width, query.height) {
            (None, None) => {
                data = medium.data.clone();
            }
            (Some(width), Some(height)) => {
                match (media_resize_steps.contains(&width), media_resize_steps.contains(&height)) {
                    (true, true) => {
                        return redirect_to(To::Width(width));
                    }
                    (false, true) => {
                        return redirect_to(To::Height(height));
                    }
                    (true, false) => {
                        return redirect_to(To::Width(width));
                    }
                    (false, false) => {
                        return redirect_to(To::Width(media_resize_steps.closest(&width)));
                    }
                }
            }
            (Some(width), None) => {
                if media_resize_steps.contains(&width) {
                    data = medium.get_resize(Some(width), None, None).await;
                } else {
                    return redirect_to(To::Width(media_resize_steps.closest(&width)));
                }
            }
            (None, Some(height)) => {
                if media_resize_steps.contains(&height) {
                    data = medium.get_resize(None, Some(height), None).await;
                } else {
                    return redirect_to(To::Height(media_resize_steps.closest(&height)));
                }
            }
        }
    }

    if let Some(data) = data {
        if let Some(mime_type) = &medium.mime_type {
            let mut headers = HeaderMap::new();
            headers.insert(CONTENT_TYPE, mime_type.parse().unwrap());
            headers.insert(
                CACHE_CONTROL,
                "max-age: 31536000".try_into().unwrap(),
            );
            headers.insert(
                LAST_MODIFIED,
                medium.updated_at.header_value.clone(),
            );
            // TODO: check IfModifiedSince, if true, check updated_at, then send empty body or else data.
            return (StatusCode::OK, headers, data.to_vec()).into_response();
        }
    }
    
    return redirect_to_index(base_url);
}

#[inline]
fn redirect_to_index(base_url: &Arc<BaseUrl>) -> Response {
    let base_url: String = (**base_url).clone().into();

    match (random::<u8>() % 4) + 1 {
        1 => Redirect::moved_permanent(base_url),
        2 => Redirect::permanent(base_url),
        3 => Redirect::see_other(base_url),
        _ => Redirect::temporary(base_url),
    }
    .into_response()
}

/// Handles requests for `/:slug`.
#[handler]
async fn find(
    Path(slug): Path<String>,
    req: &Request,
    _templates: Data<&Arc<Tera>>,
    repo: Data<&Arc<Repo>>,
    _session: &Session,
    base_url: Data<&Arc<BaseUrl>>,
    media_resize_steps: Data<&Arc<MediaResizeSteps>>,
) -> Response {
    let maybe_slug = repo.get_slug(&slug).await;

    if let Some(slug_with_item) = maybe_slug {
        let (_translation_code, slug_item) = &*slug_with_item;

        match slug_item {
            SlugItem::Page(ref code) => {
                let mut headers = HeaderMap::new();
                headers.insert(CONTENT_TYPE, HeaderValue::from_static("text/html"));
                let response =
                    (StatusCode::OK, headers, format!("<pre>{code}</pre>")).into_response();

                match code.as_str() {
                    "order" => response,
                    "legal" => response,
                    "subscription" => response,
                    _ => response,
                }
            }
            SlugItem::Medium(medium_id) => {
                let medium = repo.get_medium(*medium_id).await;
                if let Some(medium) = medium {                    
                    match *medium.item {
                        MediumItem::Other => redirect_to_index(*base_url),
                        MediumItem::Image { .. } => media_ok_or_redirect(
                            &req,
                            *base_url,
                            slug,
                            medium,
                            *media_resize_steps,
                        ).await,
                    }
                } else {
                    redirect_to_index(*base_url)
                }
            }
            _slug_item => redirect_to_index(*base_url),
        }
    } else {
        redirect_to_index(*base_url)
    }
}

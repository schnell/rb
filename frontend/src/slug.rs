//! Slug types and functions.  
//!   
//! You may want to store [Slug] and [SlugItem] as a key-value pair in a collection.

use crate::{translation::TranslationCode, Code, Error, Id};
use indoc::indoc;
use sqlx::{query_as, FromRow, PgPool};

/// Key of slug related entities.
pub type Slug = String;

/// Item as value to be stored in a key-value pair along with a [Slug] as key.
#[derive(Clone, Debug)]
pub enum SlugItem {
    Locale(Code),
    Page(Code),
    Region(Code),
    Medium(Id),
    Product(Code),
    Producer(Code),
    ProductGroup(Code),
    Property(Code),
    RatingParameter(Code),
    Constituent(Id),
}

/// Select Record for a slug. Does not contain [Slug] as key.
#[derive(Debug, FromRow)]
pub struct SlugSelect {
    pub slug: String,
    pub translation_code: Option<Code>,
    pub locale_code: Option<Code>,
    pub page_code: Option<Code>,
    pub region_code: Option<Code>,
    pub medium_id: Option<Id>,
    pub product_code: Option<Code>,
    pub producer_code: Option<Code>,
    pub product_group_code: Option<Code>,
    pub property_code: Option<Code>,
    pub rating_parameter_code: Option<Code>,
    pub constituent_id: Option<Id>,
}

/// Base statement for selecting slugs.
pub static SLUG_SELECT: &str = indoc! {"
    SELECT
        slug,
        translation_code,
        
        locale_code,
        page_code,
        region_code,
        medium_id,
        product_code,
        producer_code,
        product_group_code,
        property_code,
        rating_parameter_code,
        constituent_id
    FROM
        slugs
"};

/// Select record for a slug with item.
#[derive(Debug)]
pub struct SlugGet {
    pub slug: Slug,
    pub item: SlugItem,
    pub translation_code: Option<TranslationCode>,
}

/// Fetches all slugs from databases.
pub async fn try_get_all_slugs(pg_pool: &PgPool) -> Result<Vec<SlugGet>, Error> {
    let rows = query_as::<_, SlugSelect>(SLUG_SELECT)
        .fetch_all(pg_pool)
        .await?;

    Ok(parse_rows(rows))
}

/// Parses rows into [SlugGet] records.
fn parse_rows(rows: Vec<SlugSelect>) -> Vec<SlugGet> {
    let mut slugs = Vec::new();

    for row in rows {
        let mut item = Option::None;

        if row.locale_code.is_some() {
            item = Some(SlugItem::Locale(row.locale_code.unwrap()));
        } else if row.page_code.is_some() {
            item = Some(SlugItem::Page(row.page_code.unwrap()));
        } else if row.medium_id.is_some() {
            item = Some(SlugItem::Medium(row.medium_id.unwrap()))
        } else if row.region_code.is_some() {
            item = Some(SlugItem::Region(row.region_code.unwrap()));
        } else if row.product_code.is_some() {
            item = Some(SlugItem::Product(row.product_code.unwrap()));
        } else if row.producer_code.is_some() {
            item = Some(SlugItem::Producer(row.producer_code.unwrap()));
        } else if row.product_group_code.is_some() {
            item = Some(SlugItem::ProductGroup(row.product_group_code.unwrap()));
        } else if row.property_code.is_some() {
            item = Some(SlugItem::Property(row.property_code.unwrap()));
        } else if row.rating_parameter_code.is_some() {
            item = Some(SlugItem::RatingParameter(
                row.rating_parameter_code.unwrap(),
            ));
        } else if row.constituent_id.is_some() {
            item = Some(SlugItem::Constituent(row.constituent_id.unwrap()));
        }

        if let Some(item) = item {
            slugs.push(SlugGet {
                slug: row.slug,
                item,
                translation_code: row.translation_code,
            });
        }
    }

    slugs
}

/// Select record for a [SlugItem] with an optional [TranslationCode]. Does not contain [Slug] as key.
#[derive(Debug)]
pub struct SlugGetOne {
    pub item: SlugItem,
    pub translation_code: Option<TranslationCode>,
}

/// Fetches a slug from database for the given slug as key.
pub async fn try_get_slug<S: AsRef<str>>(
    pg_pool: &PgPool,
    slug: S,
) -> Result<Option<SlugGetOne>, Error> {
    let slug = slug.as_ref();

    let row = query_as::<_, SlugSelect>(&(SLUG_SELECT.to_owned() + "\nWHERE slug = $1"))
        .bind(slug)
        .fetch_one(pg_pool)
        .await?;

    let mut slugs = parse_rows(vec![row]);

    if let Some(SlugGet {
        item,
        translation_code,
        ..
    }) = slugs.pop()
    {
        Ok(Some(SlugGetOne {
            item,
            translation_code,
        }))
    } else {
        Ok(None)
    }
}

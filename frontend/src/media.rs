//! Media types and functions.

use crate::{text::TextsDefault, time::DateTime, BinaryData, Error, Id};

use dashmap::DashMap;
use image::{self, imageops::FilterType::Lanczos3, DynamicImage, ImageFormat};
use indoc::indoc;
use infer as mime_infer;
use moka::future::Cache as FutureCache;
use serde_json::{from_value, Value};
use sha2::{Digest, Sha256};
use sqlx::{query_as, types::time::PrimitiveDateTime as SqlxPrimitiveDateTime, FromRow, PgPool};
use std::{
    default::default,
    fs::File,
    io::{Cursor, Read},
    path::{Path, PathBuf},
    str::FromStr,
    sync::Arc,
    time::Duration,
    time::SystemTime,
};

/// Record to be stored as key in a key-value pair of itself and [Medium].
pub type MediumId = Id;

/// Item of a medium.
#[derive(Clone)]
pub enum MediumItem {
    Other,
    Image {
        image: DynamicImage,
        format: ImageFormat,
    },
    // Video(?),
}

pub type Width = u32;
pub type Height = u32;
pub type Depth = u32;

/// Resized versions of a medium.
pub type MediaResizes = FutureCache<(Width, Height), Arc<BinaryData>>;

/// Record to be stored as value in a key-value pair of [MediumId] and itself.
pub struct Medium {
    pub data: Option<Arc<BinaryData>>,
    pub item: Arc<MediumItem>,
    pub path: Option<PathBuf>,
    pub hash: Option<String>,
    pub size: Option<usize>,
    pub mime_type: Option<String>,
    pub updated_at: DateTime,
    pub texts: TextsDefault,
    resizes: Arc<MediaResizes>,
}

impl Medium {
    /// Tries to read a resized version from cache or creates a new resized version.  
    /// Preserves the aspect ratio.  
    /// Tries to best fit into `width` and `height`.  
    /// Ignores `depth` for now until 3D media are supported one day.  
    /// Returns `None` if [item][Medium::item] is [Other][MediumItem::Other].  
    /// Returns `None` if `width` and `height` are `None`.
    pub async fn get_resize(
        &self,
        width: Option<Width>,
        height: Option<Height>,
        _depth: Option<Depth>,
    ) -> Option<Arc<BinaryData>> {
        use MediumItem::*;

        if width.is_none() && height.is_none() {
            return None;
        }

        match &*self.item {
            Other => None,
            Image { image, format } => {
                let (width, height) = match (width, height) {
                    (Some(width), Some(height)) => (width, height),
                    (Some(width), None) => (
                        width,
                        ((image.height() as f32 / image.width() as f32) * width as f32 * 100.0)
                            as u32,
                    ),
                    (None, Some(height)) => (
                        ((image.width() as f32 / image.height() as f32) * height as f32 * 100.0)
                            as u32,
                        height,
                    ),
                    _ => unreachable!(),
                };

                let image = image.clone();

                let resize = self
                    .resizes
                    .try_get_with((width, height), async move {
                        let mut buffer = Cursor::new(Vec::new());
                        image
                            .resize(width, height, Lanczos3)
                            .write_to(&mut buffer, *format)?;
                        Result::<_, Error>::Ok(Arc::new(buffer.into_inner()))
                    })
                    .await;

                Some(resize.ok()?)
            }
        }
    }
}

/// Base statement for selecting media.  
///   
/// Note: You must add `GROUP BY id` to the statement in order to use it.  
/// Unfortunately, SQL forces you to put `GROUP BY` at a certain position within a statement.  
/// Since omitted here, you can inject `WHERE`, `LIMIT` and other friends.
pub static MEDIA_SELECT: &str = indoc! {"
    SELECT
        id,
        path,
        hash,
        updated_at,
        coalesce(
            json_strip_nulls(
                json_object_agg(
                    translation_code, json_build_object(
                        'name', name,
                        'short_description', short_description,
                        'long_description', long_description,
                        'published', published,
                        'published_at', published_at
                    )
                ) FILTER (WHERE medium_id IS NOT NULL)
            )
        , '{}'::json) texts
    FROM
        media
    LEFT JOIN
        media_texts
    ON
        media_texts.medium_id = id
"};

/// Select record.
#[derive(FromRow)]
pub struct MediumSelect {
    pub id: i64,
    pub path: Option<Vec<String>>,
    pub hash: Option<String>,
    pub updated_at: Option<SqlxPrimitiveDateTime>,
    pub texts: Value,
}

/// Record returned from [try_get_media].
pub struct MediumGet {
    pub id: i64,
    pub medium: Medium,
}

/// Update record for the database where the `hash` column is not set or differs from the actual file.
pub struct MediumUpdateHash {
    pub id: i64,
    pub hash: String,
}

/// Fetches media records from database and read files from `media_volume`.  
///    
/// The functions returns an error when the records cannot be fetched from the database.  
/// If `ids` is empty, all records are selected, otherwise the given IDs will be tried to fetched.  
/// If `limit` and `page` is empty, pagination is turned off and all records are selected.  
pub async fn try_get_media<P: AsRef<Path>>(
    pg_pool: &PgPool,
    media_volume: P,
    ids: Vec<Id>,
    limit: Option<u32>,
    page: Option<u32>,
    // TODO: respect this flag and update hashes where they differ for file and row.
    _sync_hashes: bool,
) -> Result<Vec<(MediumGet, Vec<Error>)>, Error> {
    let with_ids = !ids.is_empty();
    let with_pagination = limit.is_some() && page.is_some();

    let mut query = MEDIA_SELECT.to_owned();

    if with_ids {
        query += "\nWHERE id = any($1)";
    }

    query += "\nGROUP BY id";

    if with_pagination {
        query += "\nLIMIT $2\nOFFSET $3";
    }

    let mut query_as = query_as::<_, MediumSelect>(&query);

    if with_ids {
        query_as = query_as.bind(ids);
    }

    if with_pagination {
        let mut page = page.unwrap();
        if page >= 1 {
            page -= 1;
        }

        let offset = limit.unwrap() * page;
        query_as = query_as.bind(limit).bind(offset);
    }

    let rows = query_as.fetch_all(pg_pool).await?;

    let mut result = Vec::with_capacity(rows.len());
    let mut media_update_hash = Vec::new();

    for row in rows {
        let mut errors = Vec::new();

        let id = row.id;
        let mut data = None;
        let mut item = None;
        let mut hash = None;
        let mut mime_type = None;
        let mut size = None;
        let texts = from_value(row.texts).unwrap_or_else(|_| TextsDefault::new());
        let resizes = Arc::new(
            FutureCache::builder()
                .max_capacity(10)
                .time_to_live(Duration::from_secs(60 * 60 * 10))
                .time_to_idle(Duration::from_secs(60 * 60))
                .build(),
        );

        let updated_at = {
            if let Some(row_updated_at) = row.updated_at {
                DateTime::from(SystemTime::from(row_updated_at))
            } else {
                SystemTime::now().into()
            }
        };

        let path = { row.path.map(|path| PathBuf::from_iter(path.iter())) };

        if let Some(path) = &path {
            let path = media_volume.as_ref().join(path);
            let file_open = File::open(path);

            match file_open {
                Err(err) => errors.push(err.into()),
                Ok(mut file) => {
                    let mut file_contents = Vec::new();
                    let file_read = file.read_to_end(&mut file_contents);

                    match file_read {
                        Err(err) => errors.push(err.into()),
                        Ok(file_size) => {
                            size = Some(file_size);
                            let file_hash = Sha256::digest(&file_contents);
                            let file_hash = format!("{file_hash:x}");
                            let file_mime_type = mime_infer::get(&file_contents[..4]);

                            if let Some(file_mime_type) = file_mime_type {
                                let file_mime_type = file_mime_type.mime_type().to_string();

                                if let Some(image_format) =
                                    ImageFormat::from_mime_type(&file_mime_type)
                                {
                                    if let Ok(image) = image::load_from_memory_with_format(
                                        &file_contents,
                                        image_format,
                                    ) {
                                        item = Some(Arc::new(MediumItem::Image {
                                            image,
                                            format: image_format,
                                        }));
                                    }
                                }

                                data = Some(Arc::new(file_contents));
                                mime_type = Some(file_mime_type);
                            }

                            if let Some(row_hash) = row.hash {
                                if file_hash != row_hash {
                                    media_update_hash.push(MediumUpdateHash {
                                        id,
                                        hash: file_hash.clone(),
                                    });
                                }
                            }

                            hash = Some(file_hash);
                        }
                    }
                }
            }
        }

        let item = item.unwrap_or_else(|| Arc::new(MediumItem::Other));

        let medium = Medium {
            data,
            item,
            path,
            hash,
            size,
            mime_type,
            updated_at,
            texts,
            resizes,
        };

        result.push((MediumGet { id, medium }, errors));
    }

    // TODO: update hashes into database
    // if sync_hashes { media_update_hash }

    Ok(result)
}

type Step = u32;
type InputLength = u32;

#[derive(Clone, Debug, Default)]
pub struct MediaResizeSteps {
    steps: Vec<Step>,
    lookup_table: Vec<u32>,
}

impl MediaResizeSteps {
    /// Checks if `input_length` is a step in collection.
    pub fn contains(&self, input_length: &u32) -> bool {
        self.steps.contains(input_length)
    }

    /// Find the closest step for the given `input_length`.  
    /// Panics if there are less than 2 steps available.
    pub fn closest(&self, input_length: &u32) -> u32 {
        let first_step = self.steps[0];
        let last_step = self.steps[self.steps.len() - 1];

        if input_length < &first_step {
            return first_step;
        }

        if input_length > &last_step {
            return last_step;
        }

        self.lookup_table[(last_step - input_length) as usize]
    }
}

// Returns element closest to `input_length` in `steps`.
//
// Stolen-ported from: https://www.geeksforgeeks.org/find-closest-number-array/
fn closest(steps: &Vec<u32>, input_length: u32) -> u32 {
    fn get_closest(low: u32, high: u32, input_length: u32) -> u32 {
        if input_length - low >= high - input_length {
            return high;
        } else {
            return low;
        }
    }

    let n = steps.len();

    if input_length <= steps[0] {
        return steps[0];
    }
    if input_length >= steps[n - 1] {
        return steps[n - 1];
    }

    let mut i: u32 = 0;
    let mut mid: usize = 0;
    let mut j: u32 = n as u32;

    while i < j {
        mid = ((i as f32 + j as f32) / 2.0) as usize;

        if steps[mid] == input_length {
            return steps[mid];
        }

        if input_length < steps[mid] {
            if mid > 0 && input_length > steps[mid - 1] {
                return get_closest(steps[mid - 1], steps[mid], input_length);
            }

            j = mid as u32;
        } else {
            if mid < n - 1 && input_length < steps[mid + 1] {
                return get_closest(steps[mid], steps[mid + 1], input_length);
            }
            i = mid as u32 + 1;
        }
    }

    return steps[mid];
}

impl FromIterator<u32> for MediaResizeSteps {
    fn from_iter<I: IntoIterator<Item = u32>>(iter: I) -> Self {
        let mut steps: Vec<u32> = iter.into_iter().collect();
        steps.sort();
        steps.dedup();
        steps.shrink_to_fit();

        let n = steps.len();
        let first_step = steps[0];
        let last_step = steps[n - 1];
        let between_steps = first_step..=last_step;
        let mut lookup_table = Vec::with_capacity(between_steps.count());

        for between_step in first_step..=last_step {
            lookup_table.push(closest(&steps, between_step));
        }

        Self {
            steps,
            lookup_table,
        }
    }
}

impl TryFrom<&String> for MediaResizeSteps {
    type Error = Error;

    fn try_from(text: &String) -> Result<Self, Self::Error> {
        let mut steps = Vec::new();

        for step in text.split(':') {
            let _ = steps.push(u32::from_str(step)?);
        }

        Ok(Self::from_iter(steps))
    }
}

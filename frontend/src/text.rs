//! Text types.  
//!   
//! For translated texts you want to use [TextsDefault] as part of a record.

use crate::translation::TranslationCode;
use dashmap::DashMap;
use serde::{Deserialize, Serialize};
use std::time::SystemTime;

/// Text part stored in a collection of a record.
#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct TextDefault {
    pub name: String,
    pub short_description: Option<String>,
    pub long_description: Option<String>,
    pub published: bool,
    pub published_at: Option<SystemTime>,
}

/// Texts stored as a collection within a record.
pub type TextsDefault = DashMap<TranslationCode, TextDefault>;

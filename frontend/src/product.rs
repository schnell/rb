//! Product types and functions.

use crate::{Decimal, Code, media::MediumId, text::TextsDefault, time::DateTime, translation::TranslationCode};
use indoc::indoc;
use serde::Deserialize;
use dashmap::DashMap;
use sqlx::{PgPool, types::BigDecimal};

pub type ProductCode = Code;

pub struct Product {
    pub path: Vec<Code>,
    pub unit_code: Code,
    pub base_value: String,
    pub published: bool,
    pub published_at: Option<DateTime>,
    pub texts: TextsDefault,
    pub media: Vec<MediumId>,
    pub groups: Vec<ProductGroupCode>,
    pub prices: DashMap<RegionCode, Price>,
    pub properties: Properties,
    pub rating: DashMap<RatingParameterCode, RatingValue>,
    pub constituents: Vec<Constituent>,
}

pub struct Constituent {
    pub depth: u32,
    pub position: u32,
    pub notes: Vec<NoteCode>,
    pub properties: Properties,
}

pub struct Constituents(Vec<Constituent>);

impl Constituents {
    pub fn push(&mut self, depth: u32) {
        todo!()
    }

    pub fn is_valid(&self) -> bool {
        todo!()
    }
}

static PRODUCT_SELECT: &str = indoc! {"
"};

pub type ProductGroupCode = Code;

#[derive(Deserialize)]
pub struct ProductGroup {
    // TODO
}

pub type NoteCode = Code;
pub type RatingParameterCode = Code;
pub type RatingValue = Decimal;

pub struct Price {}
pub struct Taxes {}

pub struct Currency {}

pub struct Stock {}

pub type RegionCode = Code;
pub struct Region {}

pub type PropertyCode = Code;
pub type Properties = DashMap<PropertyCode, PropertyValue>;

#[derive(Debug)]
pub enum PropertyValue {
    Decimal(Decimal),
    Integer(i128),
    Bool(bool),
    Text(DashMap<TranslationCode, String>),
    TextList(DashMap<TranslationCode, String>),
    Date(DateTime),
    Code(Code),
    Point((Decimal, Decimal)),
}
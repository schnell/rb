//! Translation types.

use crate::Code;
use lazy_static::lazy_static;
use smallvec::SmallVec;
use std::str::FromStr;
use unic_langid::{langid, LanguageIdentifier};

/// Translation code to be used as a key for other items.
pub type TranslationCode = Code;
/// Another type for efficiently storing a language code.
pub type LanguageArray = SmallVec<[LanguageIdentifier; 8]>;

lazy_static! {
    /// Undefined [LanguageIdentifier].
    pub static ref LANG_ID_UNDEFINED: LanguageIdentifier = langid!("und");
}

/// Parses languages from HTTP accept header.
pub fn parse_accept_languages(value: &str) -> LanguageArray {
    let mut languages = SmallVec::<[_; 8]>::new();

    for s in value.split(',').map(str::trim) {
        if let Some(res) = parse_language(s) {
            languages.push(res);
        }
    }

    languages.sort_by(|(_, a), (_, b)| b.cmp(a));
    languages
        .into_iter()
        .map(|(language, _)| language)
        .collect()
}

/// Parses a single language.
pub fn parse_language(value: &str) -> Option<(LanguageIdentifier, u16)> {
    let mut parts = value.split(';');
    let name = parts.next()?.trim();
    let quality = match parts.next() {
        Some(quality) => parse_quality(quality).unwrap_or_default(),
        None => 1000,
    };
    let language = LanguageIdentifier::from_str(name).ok()?;
    Some((language, quality))
}

/// Calculates quality of parsing.
pub fn parse_quality(value: &str) -> Option<u16> {
    let mut parts = value.split('=');
    let name = parts.next()?.trim();
    if name != "q" {
        return None;
    }
    let q = parts.next()?.trim().parse::<f32>().ok()?;
    Some((q.clamp(0.0, 1.0) * 1000.0) as u16)
}

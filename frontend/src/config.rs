//! Types for configuring the application.

use crate::{media::MediaResizeSteps, Error};
use bytesize::ByteSize;
use log;
use poem::{
    http::Uri,
    listener::{RustlsCertificate, RustlsConfig},
};
use std::{
    collections::{HashMap, HashSet},
    env,
    net::IpAddr,
    path::PathBuf,
    str::FromStr,
};

/// Configuration for the whole application.
#[derive(Clone, Debug)]
pub struct Config {
    pub shop: HttpsConfig,
    pub editor: HttpsConfig,
    pub database_url: Uri,
    pub log_level: log::LevelFilter,
    pub media_volume: PathBuf,
    pub media_resize_steps: MediaResizeSteps,
    /// Overall cache size in bytes.
    pub cache_size_max: u64,
}

impl Config {
    /// Read config from environmental variables.
    pub fn try_from_env() -> Result<Self, Error> {
        let https_services = ["SHOP", "EDITOR"];
        let mut https_configs = HashMap::new();

        for service in https_services {
            https_configs.insert(
                service,
                HttpsConfig {
                    ip: IpAddr::from_str(&env::var(&format!("{service}_IP"))?)?,

                    port: u16::from_str(&env::var(&format!("{service}_PORT"))?)?,

                    hostname: env::var(&format!("{service}_HOSTNAME"))?,

                    cert_file: PathBuf::from(&env::var(&format!("{service}_CERT"))?),
                    key_file: PathBuf::from(&env::var(&format!("{service}_KEY"))?),
                },
            );
        }

        Ok(Self {
            shop: https_configs.remove("SHOP").unwrap(),
            editor: https_configs.remove("EDITOR").unwrap(),
            database_url: Uri::from_str(&env::var("DATABASE_URL")?)?,
            log_level: log::LevelFilter::from_str(&env::var("LOG_LEVEL")?)?,
            media_volume: PathBuf::from(&env::var("MEDIA_VOLUME")?),
            media_resize_steps: MediaResizeSteps::try_from(&env::var("MEDIA_RESIZES")?)?,
            cache_size_max: ByteSize::from_str(&env::var("FRONTEND_CACHE_SIZE_MAX")?)
                .map_err(|err| Error::ParseByteSize(err))?
                .as_u64(),
        })
    }
}

/// Configuration for HTTPS endpoints.
#[derive(Clone, Debug)]
pub struct HttpsConfig {
    pub ip: IpAddr,
    pub port: u16,
    pub hostname: String,
    pub cert_file: PathBuf,
    pub key_file: PathBuf,
}

/// Contains the hostname. Wrapped as a struct to be used as data for an [Endpoint][poem::endpoint::EndpointExt::with].
#[derive(Clone, Debug, Hash, Eq, PartialEq)]
pub struct Host(pub String);

impl From<Host> for String {
    fn from(host: Host) -> String {
        host.0
    }
}

/// Contains the base URL. Wrapped as a struct to be used as data for an [Endpoint][poem::endpoint::EndpointExt::with].
#[derive(Clone, Debug, Hash, Eq, PartialEq)]
pub struct BaseUrl(pub String);

impl From<BaseUrl> for String {
    fn from(base_url: BaseUrl) -> String {
        base_url.0
    }
}

impl HttpsConfig {
    /// Loads TLS configuration.
    pub fn load_tls(&self) -> Result<RustlsConfig, std::io::Error> {
        Ok(RustlsConfig::new().fallback(
            RustlsCertificate::new()
                .cert(std::fs::read(self.cert_file.clone())?)
                .key(std::fs::read(self.key_file.clone())?),
        ))
    }

    /// Returns the hostname.
    pub fn host(&self) -> Host {
        Host(self.hostname.clone() + ":" + &self.port.to_string())
    }

    /// Returns the base URL.
    pub fn base_url(&self) -> BaseUrl {
        let mut base_url: String = "https://".to_owned() + &self.hostname;

        if self.port != 443 {
            base_url = base_url + ":" + &self.port.to_string();
        }

        base_url += "/";

        BaseUrl(base_url)
    }

    /// Checks if two endpoints need to served on the same socket due to same IP and Port.
    pub fn same_socket(&self, other: &Self) -> bool {
        self.port == other.port && self.ip == other.ip
    }

    /// Checks if two endpoints have the same hostname.
    pub fn same_host(&self, other: &Self) -> bool {
        self.hostname == other.hostname
    }
}

impl From<HttpsConfig> for Uri {
    fn from(config: HttpsConfig) -> Self {
        let mut uri = Uri::builder().scheme("https").path_and_query("/");

        if config.port == 443 {
            uri = uri.authority(config.hostname)
        } else {
            uri = uri.authority(format!("{}:{}", config.hostname, config.port))
        }

        uri.build().unwrap()
    }
}

//! Editor endpoint created by [app] and various handler functions.

use crate::{
    config::{BaseUrl, HttpsConfig},
    repo::Repo,
    Error,
};
use poem::{
    get, handler,
    http::{header::CONTENT_TYPE, HeaderMap, HeaderValue, StatusCode},
    middleware::AddData,
    middleware::{AddDataEndpoint, CookieJarManagerEndpoint},
    session::{CookieConfig, ServerSession, ServerSessionEndpoint, Session},
    web::{cookie::SameSite, Data, IntoResponse},
    EndpointExt, Request, Response, Route,
};
use poem_dbsession::{sqlx::PgSessionStorage, DatabaseConfig};
use std::{sync::Arc, time::Duration};
use tera::Tera;

pub type EditorEndpoint = AddDataEndpoint<
    AddDataEndpoint<
        AddDataEndpoint<
            CookieJarManagerEndpoint<ServerSessionEndpoint<PgSessionStorage, Route>>,
            Arc<Tera>,
        >,
        BaseUrl,
    >,
    Arc<Repo>,
>;

/// Creates an endpoint for the editor with given configuration and dependencies.
pub async fn app(
    templates_glob: impl Into<String>,
    https_config: HttpsConfig,
    repo: Arc<Repo>,
) -> Result<EditorEndpoint, Error> {
    let templates_glob = templates_glob.into();

    let templates = Arc::new(Tera::new(&templates_glob)?);

    let base_url = https_config.base_url();

    let session_storage = PgSessionStorage::try_new(
        DatabaseConfig::new().table_name("editor_sessions"),
        (*repo.pg_pool).clone(),
    )
    .await
    .unwrap();

    let app = Route::new()
        .at("/", get(index))
        .at("/products", get(index))
        .at("/product/:code", get(index).post(index))
        .at("/media", get(index).post(index))
        .at("/medium/:slug", get(index).post(index))
        .with(ServerSession::new(
            CookieConfig::new()
                .name("session")
                .secure(true)
                .domain(https_config.host())
                .same_site(SameSite::Strict)
                .max_age(Duration::from_secs(2 * 60 * 60)),
            session_storage,
        ))
        .with(AddData::new(templates.clone()))
        .with(AddData::new(base_url))
        .with(AddData::new(repo.clone()));

    Ok(app)
}

/// Handles request for `/`.
#[handler]
async fn index(
    _req: &Request,
    _session: &Session,
    templates: Data<&Arc<Tera>>,
    _repo: Data<&Arc<Repo>>,
) -> Response {
    let context = tera::Context::new();
    let body = templates.render("index.html", &context);
    let mut headers = HeaderMap::new();
    headers.insert(CONTENT_TYPE, HeaderValue::from_static("text/html"));

    match body {
        Ok(body) => (StatusCode::OK, headers, body).into_response(),
        Err(error) => {
            return (
                StatusCode::INTERNAL_SERVER_ERROR,
                headers,
                format!("<pre>{:#?}</pre>", error),
            )
                .into_response()
        }
    }
}

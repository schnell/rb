/// [DateTime] for use with HTTP headers.
use httpdate::fmt_http_date;
use poem::http::HeaderValue;
use std::time::SystemTime;

/// DateTime wrapping [SystemTime] and a preformatted [HeaderValue] for HTTP cache headers.
#[derive(Clone, Debug)]
pub struct DateTime {
    pub system_time: SystemTime,
    pub header_value: HeaderValue,
}

impl From<SystemTime> for DateTime {
    fn from(system_time: SystemTime) -> DateTime {
        Self {
            system_time,
            header_value: fmt_http_date(system_time).try_into().unwrap(),
        }
    }
}

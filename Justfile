set dotenv-load := true
set export := true

DATA_VOLUME         := env_var("DATA_VOLUME")
MEDIA_VOLUME        := env_var("MEDIA_VOLUME")

SHOP_IP             := env_var("SHOP_IP")
SHOP_PORT           := env_var("SHOP_PORT")
SHOP_HOSTNAME       := env_var("SHOP_HOSTNAME")
SHOP_CERT           := env_var("SHOP_CERT")
SHOP_KEY            := env_var("SHOP_KEY")

EDITOR_IP           := env_var("EDITOR_IP")
EDITOR_PORT         := env_var("EDITOR_PORT")
EDITOR_HOSTNAME     := env_var("EDITOR_HOSTNAME")
EDITOR_CERT         := env_var("EDITOR_CERT")
EDITOR_KEY          := env_var("EDITOR_KEY")

DATABASE            := env_var("DATABASE")
DATABASE_IP         := env_var("DATABASE_IP")
DATABASE_PORT       := env_var("DATABASE_PORT")
DATABASE_HOSTNAME   := env_var("DATABASE_HOSTNAME")
DATABASE_USER       := env_var("DATABASE_USER")
DATABASE_PASSWORD   := env_var("DATABASE_PASSWORD")
DATABASE_URL        := "postgres://" + env_var("DATABASE_USER") + ":" + env_var("DATABASE_PASSWORD") + "@" + env_var("DATABASE_IP") + ":" + env_var("DATABASE_PORT") + "/" + env_var("DATABASE")
DATABASE_URL_INTERN := "postgres://" + env_var("DATABASE_USER") + ":" + env_var("DATABASE_PASSWORD") + "@" + env_var("DATABASE_IP") + ":" + env_var("DATABASE_PORT") + "/postgres"
DATABASE_URL_PYTHON := "postgresql+psycopg2://" + env_var("DATABASE_USER") + ":" + env_var("DATABASE_PASSWORD") + "@" + env_var("DATABASE_IP") + ":" + env_var("DATABASE_PORT") + "/" + env_var("DATABASE")

default:

init:
    #!/bin/sh
    mkdir -p $DATA_VOLUME
    mkdir -p target/
    mkdir -p tls/

gentls:
    mkdir -p tls/
    
    # Generate a passphrase
    openssl rand -base64 16384 > tls/pw

    # Generate a Private Key
    openssl genrsa -aes256 -passout file:tls/pw -out tls/key 2048

    # Generate a CSR (Certificate Signing Request)
    openssl req -new -passin file:tls/pw -key tls/key -out tls/csr \
        -subj "/C=rb/O=rb/OU=rb/CN=rb"

    # Remove passphrase from Key
    cp tls/key tls/key.org
    openssl rsa -in tls/key.org -passin file:tls/pw -out tls/key

    # Generating a self-signed certificate for many years
    openssl x509 -req -days 365000 -in tls/csr -signkey tls/key -out tls/cert

    mv tls/cert tls/cert.pem
    mv tls/key tls/key.pem

status:
    #!/bin/sh
    echo "Database: `just db status`"
    echo "Frontend: `just frontend status`"
    echo "Scheduler: `just scheduler status`"

start:
    #!/bin/sh
    just db start &> /dev/null &
    sleep 1
    just frontend start &> /dev/null &
    just scheduler start &> /dev/null &
    sleep 1
    just status

tmpfs $SUBCMD:
    #!/bin/sh

    case $SUBCMD in
    mount)
        mount -t tmpfs -o size=500m swap $DATA_VOLUME
        mount -t tmpfs -o size=6000m swap target/
        ;;
    umount)
        umount $DATA_VOLUME
        umount target/
        ;;
    esac

restart:
    #!/bin/sh
    time ( 
        just stop &> /dev/null &&
        just start
    )

stop:
    #!/bin/sh
    echo "Database: `just db stop`"
    echo "Frontend: `just frontend stop`"
    echo "Scheduler: `just scheduler stop`"
    sleep 1

reset:
    #!/bin/sh

    time (
        just stop &&
        just clean &&
        just init &&
        just build demo &&
        just db reset
    )

scheduler $SUBCMD *ARGS:
    #!/bin/sh

    case $SUBCMD in
    start)
        python scheduler.py &
        ;;
    stop)
        kill `just scheduler status`
        ;;
    status)
        ;;
    esac

clean:
    rm -rf __pycache__
    rm -rf demo/__pycache__
    rm -rf $DATA_VOLUME/*
    rm -rf target/*

logs:
    #!/bin/sh
    lnav -r $DATA_VOLUME/var/log

loc:
    cloc js/ migrations/ queries/ sass/ templates/ voc/ scheduler.py frontend/

frontend $SUBCMD:
    #!/bin/sh
    
    case $SUBCMD in
    doc)
        cargo doc -p frontend --release
        ;;
    build)
        cargo build -p frontend --release
        # strip target/release/frontend
        ;;
    check)
        cargo check -p frontend --release
        ;;
    start)
        target/release/frontend
        ;;
    stop)
        kill `just frontend status`
        ;;
    status)
        ;;
    esac

db $SUBCMD *ARGS:
    #!/bin/sh

    case $SUBCMD in
    sh)
        psql $DATABASE_URL
        ;;
    restart)
        just db stop
        just db start
        ;;
    start)
        proot \
            -b $MEDIA_VOLUME:/media \
            postgres \
                -p $DATABASE_PORT -h $DATABASE_IP \
                -D $DATA_VOLUME/var/lib/postgresql/data \
                -c unix_socket_directories=`pwd`/$DATA_VOLUME/run/postgresql \
                -c log_directory=`pwd`/$DATA_VOLUME/var/log/postgresql \
                -c logging_collector=on \
                -c log_destination='stderr' \
                -n
        ;;
    status)
        echo `head -1 $DATA_VOLUME/run/postgresql/.s.PGSQL.$DATABASE_PORT.lock 2> /dev/null`
        ;;
    stop)
        pid=`just db status 2> /dev/null`
        kill $pid 2> /dev/null
        echo $pid
        exit 0
        ;;
    init)
        mkdir -p $DATA_VOLUME/var/lib/postgresql
        mkdir -p $DATA_VOLUME/var/log/postgresql
        mkdir -p $DATA_VOLUME/run/postgresql
        mkdir -p $DATA_VOLUME/media


        echo $DATABASE_PASSWORD > $DATA_VOLUME/var/lib/postgresql/pw
        export TZ=UTC
        initdb \
            -D              $DATA_VOLUME/var/lib/postgresql/data \
            -U              $DATABASE_USER \
            --pwfile        $DATA_VOLUME/var/lib/postgresql/pw \
            --locale=C \
            --lc-collate=C \
            --lc-ctype=C \
            --encoding=UTF-8 \
            --lc-time=C \
            --auth=password
        
        rm $DATA_VOLUME/var/lib/postgresql/pw
        ;;
    setup)
        just db init
        just db start &
        # TODO watch status
        sleep 2
        just db create
        just db up
        just db stop
        ;;
    reset)
        just db clean
        just db setup
        ;;
    create)
        psql $DATABASE_URL_INTERN -c "
            CREATE DATABASE $DATABASE WITH OWNER $DATABASE_USER;
        "

        psql $DATABASE_URL -c "
            CREATE SCHEMA rb;
        "

        psql $DATABASE_URL -c "
        CREATE ROLE rb_admin nologin;
        CREATE ROLE rb_public nologin;
        CREATE ROLE rb_editor nologin;
        CREATE ROLE rb_customer nologin;

        GRANT ALL
        ON ALL TABLES
        IN SCHEMA rb
        TO rb_admin;
        "
        ;;
    up)
        sqlx migrate --source migrations run
        ;;
    down)
        sqlx migrate --source migrations revert
        ;;
    drop)
        psql $DATABASE_URL_INTERN -c "
            DROP SCHEMA rb CASCADE;
        " 2> /dev/null

        psql $DATABASE_URL_INTERN -c "
            DROP ROLE rb_customer;
        " 2> /dev/null

        psql $DATABASE_URL_INTERN -c "
            DROP ROLE rb_public;
        " 2> /dev/null

        psql $DATABASE_URL_INTERN -c "
            DROP ROLE rb_editor;
        " 2> /dev/null

        psql $DATABASE_URL_INTERN -c "
            DROP ROLE rb_admin;
        " 2> /dev/null

        psql $DATABASE_URL_INTERN -c "
            DROP DATABASE rb;
        " 2> /dev/null
        ;;
    clean)
        just db stop 2> /dev/null
        rm -rf $DATA_VOLUME/var/lib/postgresql/data/*
        ;;
    diagram)
        eralchemy -i $DATABASE_URL_PYTHON -o target/db_relational_diagram.png # --include-columns ""
        ;;
    url)
        echo $DATABASE_URL
        ;;
    esac

#!/usr/bin/python3

import schedule
import time
from os import system

def psql(sql_file_base, transaction=True):
    if transaction:
        system("psql -1 -f queries/" + sql_file + ".sql")
    else:
        system("psql    -f queries/" + sql_file)

def delete_expired_sessions():
    print("Deleting expired sessions from database: ")
    psql("delete_expired_sessions")

def delete_expired_password_reset_codes():
    print("Cleaning expired customer password reset codes from database: ")
    psql("clean_expired_customer_password_reset_codes")

def anonymize_expired_customer_data():
    print("Anonymizing expired customer accounts from database: ")
    psql("anonymize_expired_customer_data")

schedule.every(1).minutes.do(delete_expired_sessions)
schedule.every(1).hours.do(delete_expired_password_reset_codes)
schedule.every(1).hours.do(anonymize_expired_customer_data)

while True:
    schedule.run_pending()
    time.sleep(1)